﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace lms_system.APIHelper
{
    public static class LMSProductAPI
    {
        private enum ApiUrls
        {
            login, getAllEmployees, createEmployee, deleteEmployee, getAllBranches, createBranch, deleteBranch, getAllRoles, createRole, deleteRole, getAllDesignations,
            createDesignation, deleteDesignation, getActiveRoles, getActiveBranches, getDesignations
        }
        private static string GetCommonUrl()
        {
            return "http://103.44.53.3:9090/";
        }
        public static string PostLMSAPI(string postType, string postUrl, string requestJson, ref string msg, string tokenKey = null)
        {
            string ReturnValue = string.Empty;

            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(postUrl);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                Http.Method = postType;
                byte[] lbPostBuffer = null;
                if (!string.IsNullOrEmpty(requestJson))
                {
                    lbPostBuffer = Encoding.UTF8.GetBytes(requestJson);
                    Http.ContentLength = lbPostBuffer.Length;
                }
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";
                if (!string.IsNullOrEmpty(tokenKey))
                {
                    Http.Headers["Authorization"] = tokenKey; //"Bearer " + tokenKey;
                }

                if (!string.IsNullOrEmpty(requestJson))
                {
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }

            return ReturnValue;
        }
        public static string GetAuthenticate(string username, string password, ref string msg)
        {
            string url = GetCommonUrl() + ApiUrls.login;
            string requestJson = "{\"username\": \"" + username + "\",\"password\": \"" + password + "\"}";
            return PostLMSAPI("POST", url, requestJson, ref msg, null);
        }

        #region [Employee Section]   
        public static string GetAllActiveEmployeeList(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getAllEmployees";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string GetNonAppUserEmployeesList(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getNonAppUserEmployees";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string InsertUpdateEmployeeDetail(string empPara, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "createEmployee";
            return PostLMSAPI("POST", url, empPara, ref msg, access_token);
        }
        public static string DeleteEmployeeDetail(string code, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "deleteEmployee?employeeid=" + code;
            return PostLMSAPI("DELETE", url, "", ref msg, access_token);
        }
        #endregion

        #region [Privilege Section]
        public static string GetAllActiveBranchList(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getAllBranches";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string InsertBranchDetail(string branchPara, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "createBranch";
            return PostLMSAPI("POST", url, branchPara, ref msg, access_token);
        }
        public static string DeleteBranchDetail(string code, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "deleteBranch?branchCode=" + code;
            return PostLMSAPI("DELETE", url, "", ref msg, access_token);
        }
        public static string GetAllActiveRoleList(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getAllRoles";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string InsertRoleDetail(string rolePara, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "createRole";
            return PostLMSAPI("POST", url, rolePara, ref msg, access_token);
        }
        public static string InsertCreateLoginDetail(string rolePara, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "createLogin";
            return PostLMSAPI("POST", url, rolePara, ref msg, access_token);
        }
        public static string GetCreateLoginDetails(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getAllAppUsers";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string DeleteRoleDetail(string code, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "deleteRole?rolecode=" + code;
            return PostLMSAPI("DELETE", url, "", ref msg, access_token);
        }
        public static string GetRolePermissionsByRoleId(string roleId, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getPermissionByRole?roleId=" + roleId;
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string GetAllActiveDesignationList(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getAllDesignations";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string InsertDesignationDetail(string descPara, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "createDesignation";
            return PostLMSAPI("POST", url, descPara, ref msg, access_token);
        }
        public static string DeleteDesignationDetail(string code, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "deleteDesignation?designationcode=" + code;
            return PostLMSAPI("DELETE", url, "", ref msg, access_token);
        }
        public static string GetRoleForDropdown(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getActiveRoles";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string GetBranchForDropdown(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getActiveBranches";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string GetDesignationForDropdown(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getDesignations";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string UpdateRolePermission(string access_token, string reqstr, ref string msg)
        {
            string url = GetCommonUrl() + "updateRolePermission";
            return PostLMSAPI("POST", url, reqstr, ref msg, access_token);
        }
        #endregion
        //public static void InitializeAccessTokenSession(string accessToken)
        //{
        //    if (!string.IsNullOrEmpty(accessToken))
        //    {
        //        List<string> objAccessToken = new List<string>();
        //        objAccessToken.Add(accessToken);
        //        objAccessToken.Add(DateTime.Now.ToString());
        //        objAccessToken.Add("exist");
        //        HttpContext.Current.Session["accessToken"] = objAccessToken;
        //    }
        //}
        //public static bool IsTokenExist(ref string token)
        //{
        //    if (HttpContext.Current.Session["accessToken"] != null)
        //    {
        //        List<string> objAccessToken = (List<string>)HttpContext.Current.Session["accessToken"];
        //        TimeSpan TS = DateTime.Now - Convert.ToDateTime(objAccessToken[1]);
        //        if (TS.Minutes < 1440)
        //        {
        //            token = objAccessToken[0];
        //            return true;
        //        }
        //        else
        //        {
        //            HttpContext.Current.Session["accessToken"] = null;
        //        }
        //    }

        //    return false;
        //}

        #region [Lead Section]
        public static string GetProductList(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getAllProducts";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        public static string InsertProductDetail(string productPara, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "createProduct";
            return PostLMSAPI("POST", url, productPara, ref msg, access_token);
        }
        public static string InsertLeadDetail(string productPara, string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "createLeadData";
            return PostLMSAPI("POST", url, productPara, ref msg, access_token);
        }
        public static string GetLeadList(string access_token, ref string msg)
        {
            string url = GetCommonUrl() + "getAllLeads";
            return PostLMSAPI("GET", url, "", ref msg, access_token);
        }
        #endregion
    }
}