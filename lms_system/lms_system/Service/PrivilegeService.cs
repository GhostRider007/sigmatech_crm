﻿using lms_system.Helper;
using lms_system.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace lms_system.Service
{
    public static class PrivilegeService
    {
        public static List<BranchModel> GetAllActiveBranchList()
        {
            return PrivilegeHelper.GetAllActiveBranchList();
        }
        public static List<string> InsertBranchDetail(BranchModel branch)
        {
            return PrivilegeHelper.InsertBranchDetail(branch);
        }
        public static List<string> ProcessToDeleteBranch(string code)
        {
            return PrivilegeHelper.ProcessToDeleteBranch(code);
        }
        public static List<RoleModel> GetAllActiveRoleList()
        {
            return PrivilegeHelper.GetAllActiveRoleList();
        }
        public static List<string> InsertRoleDetail(RoleModel role)
        {
            return PrivilegeHelper.InsertRoleDetail(role);
        }
        public static List<string> ProcessToDeleteRole(string code)
        {
            return PrivilegeHelper.ProcessToDeleteRole(code);
        }
        public static List<RolePermissionRights> GetRolePermissionsByRoleId(string roleid, ref string msg)
        {
            return PrivilegeHelper.GetRolePermissionsByRoleId(roleid, ref msg);
        }
        public static List<DesignationModel> GetAllActiveDesignationList()
        {
            return PrivilegeHelper.GetAllActiveDesignationList();
        }
        public static List<string> InsertDesignationDetail(DesignationModel designation)
        {
            return PrivilegeHelper.InsertDesignationDetail(designation);
        }
        public static List<string> ProcessToDeleteDesignation(string code)
        {
            return PrivilegeHelper.ProcessToDeleteDesignation(code);
        }
        public static List<RoleModel> GetRoleForDropdown()
        {
            return PrivilegeHelper.GetRoleForDropdown();
        }
        public static List<BranchModel> GetBranchForDropdown()
        {
            return PrivilegeHelper.GetBranchForDropdown();
        }
        public static List<DesignationModel> GetDesignationForDropdown()
        {
            return PrivilegeHelper.GetDesignationForDropdown();
        }
        public static bool InsertCreateLoginDetail(CreateLogin model)
        {
            CreateAppUser createAppUser = PrivilegeHelper.InsertCreateLoginDetail(model);
            if (createAppUser != null)
            {
                if (createAppUser.code == "success")
                {
                    return true;
                }
            }
            return false;
        }
        public static string BindCreatLoginData()
        {
            StringBuilder sbStr = new StringBuilder();
            try
            {
                CreateLoginDetails createLogin = GetCreateLoginDetails();
                if (createLogin != null)
                {
                    if (createLogin.data != null && createLogin.data.Count > 0)
                    {
                        int loop = 0;
                        foreach (var item in createLogin.data)
                        {
                            loop = loop + 1;
                            sbStr.Append("<tr>");
                            sbStr.Append("<td class='text-center' id='tdCLEmpCode_" + item.userid + "' data-isactive='" + item.isactive + "'  data-empcode='" + item.employid + "' data-empmname='" + item.middlename + "'>" + loop + "</td>");
                            //sbStr.Append("<td id='tdCLEmpId_" + item.userid + "' >" + item.employid + " </td>");
                            sbStr.Append("<td class='text-center' id='tdCLFirstName_" + item.userid + "'>" + item.firstname + " </td>");
                            sbStr.Append("<td class='text-center' id='tdCLLastName_" + item.userid + "'>" + item.lastname + " </td>");
                            sbStr.Append("<td class='text-center' id='tdCLUserName_" + item.userid + "'>" + item.username + " </td>");
                            sbStr.Append("<td class='text-center' id='tdCLRoleName_" + item.userid + "' data-rolecode='" + item.rolecode + "' data-roleid='" + item.roleid + "'>" + item.rolename + " </td>");
                            sbStr.Append("<td class='text-center'>" + (item.isactive == 1 ? "Active" : "InActive") + "</td>");
                            sbStr.Append("<td class='text-center'>" + item.creationdate + "</td>");
                            sbStr.Append("<td class='text-center'><i style='font-size: 20px;cursor:pointer;' class='fa fa-pencil-square-o text-success' aria-hidden='true' title='Modify'");
                            sbStr.Append("onclick=ModifyCreateLoginDetail(" + item.userid + ")></i></td>");
                            sbStr.Append("</tr>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return sbStr.ToString();
        }
        public static CreateLoginDetails GetCreateLoginDetails()
        {
            return PrivilegeHelper.GetCreateLoginDetails();
        }
        public static string UpdateRolePermission(string reqstr)
        {
            return PrivilegeHelper.UpdateRolePermission(reqstr);
        }
    }
}