﻿using lms_system.Helper;
using lms_system.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lms_system.Service
{
    public static class EmployeeService
    {
        public static List<string> InsertUpdateEmployeeDetail(EmployeeModel model)
        {
            return EmployeeHelper.InsertUpdateEmployeeDetail(model);
        }
        public static List<EmployeeModel> GetAllActiveEmployeeList()
        {
            return EmployeeHelper.GetAllActiveEmployeeList();
        }
        public static List<NonAppUserEmployeesData> GetNonAppUserEmployeesList()
        {
            return EmployeeHelper.GetNonAppUserEmployeesList();
        }
        public static List<string> ProcessToDeleteEmployee(string code)
        {
            return EmployeeHelper.ProcessToDeleteEmployee(code);
        }
    }
}