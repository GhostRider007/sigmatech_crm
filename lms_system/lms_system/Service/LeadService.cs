﻿using lms_system.Helper;
using lms_system.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lms_system.Service
{
    public static class LeadService
    {
        public static List<ProductData> GetProductList()
        {
            return LeadHelper.GetProductList();
        }
        public static bool InsertProductDetail(string pname, string pcode, string pdesc, string pwsrate, ref int id)
        {
            return LeadHelper.InsertProductDetail(pname, pcode, pdesc, pwsrate, ref id);
        }

        public static bool InsertLeadDetail(CreateLead model, ref int id, ref string msg)
        {
            return LeadHelper.InsertLeadDetail(model, ref id, ref msg);
        }

        public static LeadListResponse GetLeadListResponse()
        {
            return LeadHelper.GetLeadListResponse();
        }
    }
}