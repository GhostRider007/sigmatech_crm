﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lms_system.Helper;
using lms_system.Models;

namespace lms_system.Service
{
    public static class AccountService
    {
        public static bool BackOfficeLogin(BackOfficeLogin login, ref string msg)
        {
            bool isSuccess = AccountHelper.BackOfficeLogin(login, ref msg);
            if (!string.IsNullOrEmpty(msg))
            {
                if (msg.ToLower().Contains("unauthorized"))
                {
                    msg = "User name and password not correct!" + msg;
                }
            }
            return isSuccess;
        }
        public static UserAccountDetails GetCurrentUserDetails()
        {
            return AccountHelper.GetCurrentUserDetails();
        }
        public static bool IsUserLoggedIn()
        {
            return AccountHelper.IsUserLoggedIn();
        }

        public static void LogoutUser(string actionType = null)
        {
            AccountHelper.LogoutUser(actionType);
        }
    }
}