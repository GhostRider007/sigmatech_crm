﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lms_system.Service;

namespace lms_system.Controllers
{
    public class DashboardController : Controller
    {
        public ActionResult AdminDashboard()
        {
            //if (!AccountService.IsUserLoggedIn())
            //{
            //    AccountService.LogoutUser();
            //}

            return View();
        }

        public ActionResult LogoutUser()
        {
            AccountService.LogoutUser("outer");
            return View();
        }
    }
}