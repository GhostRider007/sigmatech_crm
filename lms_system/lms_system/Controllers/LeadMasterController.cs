﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using lms_system.Models;
using lms_system.Service;

namespace lms_system.Controllers
{
    public class LeadMasterController : Controller
    {
        public ActionResult ProductMaster()
        {
            return View();
        }
        public ActionResult LeadMaster()
        {
            //LeadListResponse model = new LeadListResponse();
            //model = LeadService.GetLeadListResponse();
            //return View(model);

            return View();
        }
        public ActionResult AddEditLeadMaster()
        {
            CreateLead model = new CreateLead();
            model.ProductList = CommonService.ProductList(LeadService.GetProductList());
            return View(model);
        }
        [HttpPost]
        public ActionResult AddEditLeadMaster(CreateLead model)
        {
            int id = 0; string msg = string.Empty;
            if (LeadService.InsertLeadDetail(model, ref id, ref msg))
            {
                if (id <= 0)
                {
                    TempData["LeadMessage"] = "Lead record inserted successfully.";
                }
                else
                {
                    TempData["LeadMessage"] = "Lead record updated successfully.";
                }
            }
            return RedirectToAction("LeadMaster");
        }
        #region [Json Section]
        public JsonResult SaveProductDetail(string pname, string pcode, string pdesc, string pwsrate)
        {
            List<string> strHtml = new List<string>();

            try
            {
                int id = 0;
                if (LeadService.InsertProductDetail(pname, pcode, pdesc, pwsrate, ref id))
                {
                    strHtml.Add(GetAllProductDetail());
                    strHtml.Add(id.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(strHtml);
        }
        public JsonResult GetAllProductList()
        {
            CreateLead model = new CreateLead();
            model.ProductList = CommonService.ProductList(LeadService.GetProductList());
            return Json(model);
        }
        public JsonResult BindProductDetail()
        {
            return Json(GetAllProductDetail());
        }
        public JsonResult SaveLeadDetail(CreateLead lead)
        {
            List<string> result = new List<string>();

            int id = 0; string msg = string.Empty;
            if (LeadService.InsertLeadDetail(lead, ref id, ref msg))
            {
                result.Add(id.ToString());
                result.Add(BindAllLead());
            }
            return Json(result);
        }

        public JsonResult GetAllLeadData()
        {
            return Json(BindAllLead());
        }
        #endregion

        private string BindAllLead()
        {
            StringBuilder strHtml = new StringBuilder();
            try
            {
                LeadListResponse model = new LeadListResponse();
                model = LeadService.GetLeadListResponse();

                if (model.data != null && model.data.Count > 0)
                {
                    int loop = 0;
                    foreach (var item in model.data)
                    {
                        loop = loop + 1;
                        strHtml.Append("<tr>");
                        strHtml.Append("<td>" + @loop + "</td>");
                        strHtml.Append("<td id='tdLFirstName_" + item.leadid + "'>" + item.firstname + "</td>");
                        strHtml.Append("<td id='tdLLastName_" + item.leadid + "'>" + item.lastname + "</td>");
                        strHtml.Append("<td id='tdLEmailId_" + item.leadid + "'>" + item.emailid + "</td>");
                        strHtml.Append("<td style='display:none!important;' id='tdLPhone_" + item.leadid + "'>" + item.phone + "</td>");
                        strHtml.Append("<td id='tdLMobile_" + item.leadid + "'>" + item.mobile + "</td>");
                        strHtml.Append("<td id='tdLProduct_" + item.leadid + "'>" + item.productcode + "</td>");
                        strHtml.Append("<td id='tdLLeadfrom_" + item.leadid + "'>" + item.leadsource + "</td>");
                        strHtml.Append("<td style='display:none!important;' id='tdLCountry_" + item.leadid + "'>" + item.country + "</td>");
                        strHtml.Append("<td id='tdLState_" + item.leadid + "'>" + item.state + "</td>");
                        strHtml.Append("<td id='tdLRemark_" + item.leadid + "'>" + item.remarks + "</td>");
                        strHtml.Append("<td>" + item.creationdate + "</td>");
                        strHtml.Append("<td class='text-center'>");
                        strHtml.Append("<i style='font-size: 20px;cursor:pointer;' class='fa fa-pencil-square-o text-success' aria-hidden='true' title='Modify'");
                        strHtml.Append("onclick=ModifyLeadDetail(" + item.leadid + ")></i>");
                        strHtml.Append("</td></tr>");
                    }
                }
                else
                {
                    strHtml.Append("<tr><td colspan='11' class='text-danger text-center'>No record found!</td></tr>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return strHtml.ToString();
        }
        private string GetAllProductDetail()
        {
            StringBuilder strHtml = new StringBuilder();

            try
            {
                List<ProductData> productList = LeadService.GetProductList();
                if (productList != null && productList.Count > 0)
                {
                    foreach (var item in productList)
                    {
                        strHtml.Append("<tr>");
                        strHtml.Append("<td>" + item.id + "</td>");
                        strHtml.Append("<td>" + item.productname + "</td>");
                        strHtml.Append("<td>" + item.productcode + "</td>");
                        strHtml.Append("<td>" + item.productdesc + "</td>");
                        strHtml.Append("<td>" + item.wholesaleprice + "</td>");
                        strHtml.Append("<td class='text-center'>");
                        strHtml.Append("<i style='font-size: 20px;cursor:pointer;' class='fa fa-pencil-square-o text-success' aria-hidden='true' title='Modify'");
                        strHtml.Append(" onclick=ModifyProductDetail('" + item.productname.ToString().Replace(" ", "_") + "','" + item.productcode.ToString().Replace(" ", "_") + "','" + item.productdesc.ToString().Replace(" ", "_") + "','" + item.wholesaleprice.ToString().Replace(" ", "_") + "');></i>");
                        strHtml.Append("</td></tr>");
                    }
                }
                else
                {
                    strHtml.Append("<tr><td colspan='6' class='text-danger text-center'>No record found!</td></tr>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return strHtml.ToString();
        }
    }
}