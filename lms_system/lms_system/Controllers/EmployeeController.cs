﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lms_system.Models;
using lms_system.Service;

namespace lms_system.Controllers
{
    public class EmployeeController : Controller
    {
        public ActionResult EmployeeList()
        {
            EmployeeModel model = new EmployeeModel();
            model.EmployeeList = EmployeeService.GetAllActiveEmployeeList();
            model.RoleList = CommonService.PopulateRole(PrivilegeService.GetRoleForDropdown());
            model.BranchList = CommonService.PopulateBranch(PrivilegeService.GetBranchForDropdown());
            model.DesignationList = CommonService.PopulateDesignation(PrivilegeService.GetDesignationForDropdown());
            return View(model);
        }
        public ActionResult AddEditEmployee()
        {
            EmployeeModel model = new EmployeeModel();
            //model.RoleCode = "R001";
            model.RoleList = CommonService.PopulateRole(PrivilegeService.GetRoleForDropdown());
            model.BranchList = CommonService.PopulateBranch(PrivilegeService.GetBranchForDropdown());
            model.DesignationList = CommonService.PopulateDesignation(PrivilegeService.GetDesignationForDropdown());

            return View(model);
        }
        public JsonResult ProcessToInsertEmployee(EmployeeModel model)
        {
            return Json(EmployeeService.InsertUpdateEmployeeDetail(model));
        }
        public JsonResult ProcessToDeleteEmployee(string code)
        {
            return Json(EmployeeService.ProcessToDeleteEmployee(code));
        }
    }
}