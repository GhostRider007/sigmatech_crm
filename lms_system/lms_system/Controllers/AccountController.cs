﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using lms_system.Models;
using lms_system.Service;

namespace lms_system.Controllers
{
    public class AccountController : Controller
    {
        public JsonResult ProcessBackendLogin(string username, string password, string isrember)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    BackOfficeLogin login = new BackOfficeLogin();
                    login.UserId = username;
                    login.Password = password;
                    login.Remember = isrember.ToLower().Trim() == "true" ? true : false;

                    bool isSuccessLogin = AccountService.BackOfficeLogin(login, ref msg);
                    if (isSuccessLogin)
                    {
                        result.Add("true");
                        result.Add("/dashboard");
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
                else
                {
                    result.Add("false");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult GetMasterPageContent()
        {
            string result = string.Empty;
            UserAccountDetails currUser = AccountService.GetCurrentUserDetails();
            if (currUser != null)
            {
                if (!string.IsNullOrEmpty(currUser.userDetail.pannumber))
                {
                    StringBuilder dbStr = new StringBuilder();
                    dbStr.Append("<div class='clearfix'></div>");

                    dbStr.Append("<div class='profile clearfix'>");
                    dbStr.Append("<div class='profile_pic'><img src='/Content/frontlayout/images/user.png' class='img-circle profile_img'/></div>");
                    dbStr.Append("<div class='profile_info'><span>Welcome,</span><h2>" + currUser.userDetail.firstname + "</h2></div>");
                    dbStr.Append("</div>");
                    dbStr.Append(" <br />");

                    dbStr.Append("<div id='sidebar-menu' class='main_menu_side hidden-print main_menu'>");
                    dbStr.Append("<div class='menu_section'>");
                    dbStr.Append("<ul class='nav side-menu'>");
                    dbStr.Append("<li class='lidashboard'><a href='/dashboard'><i class='fa fa-home'></i>Dashboard</a></li>");
                    foreach (var item in currUser.authorities.OrderBy(p => p.menuablename))
                    {
                        dbStr.Append("<li><a href='" + item.uri + "'><i class='" + (item.menuicon != null ? item.menuicon : "fa fa-tasks") + "'></i>" + item.menuablename + "</a></li>");
                    }
                    dbStr.Append("</ul>");
                    dbStr.Append("</div>");
                    dbStr.Append("</div>");

                    result = dbStr.ToString();
                }
            }

            return Json(result);
        }
    }
}