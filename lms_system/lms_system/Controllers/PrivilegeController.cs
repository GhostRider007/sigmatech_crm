﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using lms_system.Models;
using lms_system.Service;

namespace lms_system.Controllers
{
    public class PrivilegeController : Controller
    {
        public ActionResult BranchList()
        {
            BranchModel model = new BranchModel();
            model.BranchList = PrivilegeService.GetAllActiveBranchList();
            return View(model);
        }
        public JsonResult ProcessToInsertUpdateBranch(BranchModel model)
        {
            return Json(PrivilegeService.InsertBranchDetail(model));
        }
        public JsonResult ProcessToDeleteBranch(string code)
        {
            return Json(PrivilegeService.ProcessToDeleteBranch(code));
        }
        public ActionResult RoleList()
        {
            RoleModel model = new RoleModel();
            model.RoleList = PrivilegeService.GetAllActiveRoleList();
            return View(model);
        }
        public ActionResult RolePermission()
        {
            RolePermission model = new RolePermission();
            model.RoleList = CommonService.PopulateRole(PrivilegeService.GetRoleForDropdown());
            //model.RoleRightsList = PrivilegeService.GetRolePermissionsByRoleId(string.Empty);
            return View(model);
        }
        public ActionResult AssignRoleList()
        {
            CreateLogin model = new CreateLogin();

            model.RoleList = CommonService.PopulateRoleId(PrivilegeService.GetRoleForDropdown());
            model.EmployeeList = CommonService.PopulateNonEmployee(EmployeeService.GetNonAppUserEmployeesList());



            return View(model);
        }
        public JsonResult ProcessToCreateLogin(CreateLogin model)
        {
            string result = string.Empty;
            if (PrivilegeService.InsertCreateLoginDetail(model))
            {
                result = PrivilegeService.BindCreatLoginData();
            }
            return Json(result);
        }
        public JsonResult ProcessToUpdateCreateLogin(CreateLogin model)
        {
            bool isSuccess = PrivilegeService.InsertCreateLoginDetail(model);
            return Json(isSuccess);
        }
        public ActionResult AssignRoleToEmployee()
        {
            CreateLogin model = new CreateLogin();

            model.RoleList = CommonService.PopulateRoleId(PrivilegeService.GetRoleForDropdown());
            model.EmployeeList = CommonService.PopulateNonEmployee(EmployeeService.GetNonAppUserEmployeesList());



            return View(model);
        }
        public JsonResult ProcessToInsertUpdateRole(RoleModel model)
        {
            return Json(PrivilegeService.InsertRoleDetail(model));
        }
        public JsonResult ProcessToDeleteRole(string code)
        {
            return Json(PrivilegeService.ProcessToDeleteRole(code));
        }
        public ActionResult DesignationList()
        {
            DesignationModel model = new DesignationModel();
            model.DesignationList = PrivilegeService.GetAllActiveDesignationList();
            return View(model);
        }
        public JsonResult ProcessToInsertUpdateDesignation(DesignationModel model)
        {
            return Json(PrivilegeService.InsertDesignationDetail(model));
        }
        public JsonResult ProcessToDeleteDesignation(string code)
        {
            return Json(PrivilegeService.ProcessToDeleteDesignation(code));
        }
        public JsonResult GetRolePermissionsByRoleId(string roleid)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(roleid))
            {
                string msg = string.Empty;

                List<RolePermissionRights> RoleRightsList = PrivilegeService.GetRolePermissionsByRoleId(roleid, ref msg);
                if (RoleRightsList != null && RoleRightsList.Count > 0)
                {
                    StringBuilder sbRole = new StringBuilder();
                    int loopCount = 0;
                    foreach (var item in RoleRightsList)
                    {
                        if (loopCount == 0)
                        {
                            sbRole.Append("<div class='row'>");
                        }

                        sbRole.Append("<div class='col-sm-3' style='padding:15px;'>");
                        //sbRole.Append("<div class='checkbox'>");
                        //sbRole.Append("<label class=''><div class='icheckbox_flat-green' style='position: relative;'>");
                        //sbRole.Append("<input type='checkbox' class='flat' " + (item.selected == "true" ? "checked='checked'" : "") + " />");
                        //sbRole.Append("<ins class='iCheck-helper' style='position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;'></ins>");
                        //sbRole.Append("</div>" + item.rightsname);
                        sbRole.Append("<label class='lblcheckbox'>" + item.rightsname);
                        sbRole.Append("<input type='checkbox' class='chkrolerightsclass' id='chkRoleRights_" + item.rightsid + "' data-rightsid='" + item.rightsid + "' " + (item.selected == "true" ? "checked='checked'" : "") + ">");
                        sbRole.Append("<span class='checkmark'></span></label>");
                        //sbRole.Append(item.rightsname);
                        //sbRole.Append("</label>");
                        //sbRole.Append("</div>");
                        sbRole.Append("</div>");
                        loopCount = loopCount + 1;

                        if (loopCount == 4)
                        {
                            loopCount = 0;
                            sbRole.Append("</div>");
                        }
                    }

                    sbRole.Append("</div><div class='un_solid'></div>");
                    sbRole.Append("<div class='row'><div class='col-sm-12 text-right'>");
                    sbRole.Append("<button id='btnRoleRightsSubmit' type='submit' class='btn btn-info only-save customer-form-submiter'>");
                    sbRole.Append("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Assign Role Rights </button>");
                    sbRole.Append("</div></div>");

                    result = sbRole.ToString();
                }
                else
                {
                    result = msg;
                }
            }

            return Json(result);
        }

        //Fetch create login data
        public JsonResult BindCreatLoginData()
        {
            return Json(PrivilegeService.BindCreatLoginData());
        }
        public JsonResult BindEmployeeAndRoleDetail()
        {
            CreateLogin model = new CreateLogin();

            model.RoleList = CommonService.PopulateRoleId(PrivilegeService.GetRoleForDropdown());
            model.EmployeeList = CommonService.PopulateNonEmployee(EmployeeService.GetNonAppUserEmployeesList());

            return Json(model);
        }
        public JsonResult UpdateRolePermission(string reqstr)
        {
            return Json(PrivilegeService.UpdateRolePermission(reqstr));
        }
    }
}