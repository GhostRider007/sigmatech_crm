﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lms_system.Models
{
    public static class CommonService
    {
        public static List<SelectListItem> PopulateRole(List<RoleModel> roleList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in roleList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.RoleName.ToString(),
                    Value = item.RoleId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateRoleId(List<RoleModel> roleList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in roleList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.RoleName.ToString(),
                    Value = item.RoleId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateBranch(List<BranchModel> branchList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in branchList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.BranchName.ToString(),
                    Value = item.BranchCode.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateDesignation(List<DesignationModel> designationList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in designationList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.DesignationName.ToString(),
                    Value = item.DesignationCode.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateNonEmployee(List<NonAppUserEmployeesData> empList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in empList)
            {
                string firstName = !string.IsNullOrEmpty(item.firstname.Trim().ToString()) ? item.firstname.Trim().ToString() : string.Empty;
                string middleName = !string.IsNullOrEmpty(item.middlename.Trim().ToString()) ? " " + item.middlename.Trim().ToString() : string.Empty;
                string lastName = !string.IsNullOrEmpty(item.lastname.Trim().ToString()) ? " " + item.lastname.Trim().ToString() : string.Empty;

                items.Add(new SelectListItem
                {
                    Value = item.empid + "," + firstName + middleName + lastName,
                    Text = item.employeeid.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> ProductList(List<ProductData> productList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in productList)
            {
                items.Add(new SelectListItem
                {
                    Value = item.productname,
                    Text = item.productcode
                });
            }

            return items;
        }
    }
}