﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lms_system.Models
{
    public class BackOfficeLogin
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; }
    }

    public class UserDetail
    {
        public object dateofbirth { get; set; }
        public string firstname { get; set; }
        public string mobilenumber { get; set; }
        public string middlename { get; set; }
        public string emailid { get; set; }
        public string pannumber { get; set; }
        public string lastname { get; set; }
    }

    public class Authority
    {
        public object menuicon { get; set; }
        public string menuablename { get; set; }
        public string rightsname { get; set; }
        public string uri { get; set; }
    }

    public class UserAccountDetails
    {
        public UserDetail userDetail { get; set; }
        public string webtoken { get; set; }
        public List<Authority> authorities { get; set; }
    }
}