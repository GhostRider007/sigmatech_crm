﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lms_system.Models
{
    #region [Branch Section]
    public class BranchModel
    {
        public int BranchId { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string BranchContactNumber { get; set; }
        public string BranchAddress { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int IsActive { get; set; }
        public List<BranchModel> BranchList { get; set; }
    }
    public class BranchDropDownData
    {
        public string branchcode { get; set; }
        public string branchname { get; set; }
    }
    public class BranchDropDown
    {
        public string code { get; set; }
        public List<BranchDropDownData> data { get; set; }
        public string message { get; set; }
    }
    public class BranchDataDetails
    {
        public int branchid { get; set; }
        public string branchaddress { get; set; }
        public string branchcode { get; set; }
        public string branchcontactno { get; set; }
        public string branchname { get; set; }
        public string createddate { get; set; }
        public int isactive { get; set; }
    }
    public class BranchDetails
    {
        public string code { get; set; }
        public List<BranchDataDetails> data { get; set; }
        public string message { get; set; }
    }
    #endregion

    #region [Role Section]
    public class RoleModel
    {
        public int RoleId { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int IsActive { get; set; }
        public List<RoleModel> RoleList { get; set; }
    }
    public class RolePermission
    {
        public string RoleCode { get; set; }
        public List<SelectListItem> RoleList { get; set; }
        //public List<RolePermissionRights> RoleRightsList { get; set; }
    }
    public class RoleDataDetails
    {
        public int roleid { get; set; }
        public string entrydate { get; set; }
        public int isactive { get; set; }
        public string rolecode { get; set; }
        public string roledescription { get; set; }
        public string rolename { get; set; }
    }
    public class RoleDetails
    {
        public string code { get; set; }
        public List<RoleDataDetails> data { get; set; }
        public string message { get; set; }
    }
    public class RoleDropDownData
    {
        public string rolecode { get; set; }
        public int roleid { get; set; }
        public string rolename { get; set; }
    }
    public class RoleDropDown
    {
        public string code { get; set; }
        public List<RoleDropDownData> data { get; set; }
        public string message { get; set; }
    }
    public class RolePermissionRights
    {
        public int rightsid { get; set; }
        public string selected { get; set; }
        public string rightsname { get; set; }
    }

    public class RolePermissionByRole
    {
        public string code { get; set; }
        public List<RolePermissionRights> data { get; set; }
        public string message { get; set; }
    }


    #endregion

    #region [Designation Section]        
    public class DesignationModel
    {
        public int DesignationId { get; set; }
        public string DesignationCode { get; set; }
        public string DesignationName { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int IsActive { get; set; }
        public List<DesignationModel> DesignationList { get; set; }
    }
    public class DesignationDropDownData
    {
        public string desigcode { get; set; }
        public string designame { get; set; }
    }
    public class DesignationDropDown
    {
        public string code { get; set; }
        public List<DesignationDropDownData> data { get; set; }
        public string message { get; set; }
    }
    public class DesignationDataDetails
    {
        public int designationid { get; set; }
        public string createddate { get; set; }
        public string desigcode { get; set; }
        public string desigdesc { get; set; }
        public string designame { get; set; }
        public int isactive { get; set; }
    }

    public class DesignationDetails
    {
        public string code { get; set; }
        public List<DesignationDataDetails> data { get; set; }
        public string message { get; set; }
    }


    #endregion

    #region [Create Login Section]
    public class AppUser
    {
        public int userid { get; set; }
        public string createdby { get; set; }
        public DateTime creationdate { get; set; }
        public int employid { get; set; }
        public int isactive { get; set; }
        public object lastlogin { get; set; }
        public int loggedin { get; set; }
        public string password { get; set; }
        public int roleid { get; set; }
        public string username { get; set; }
    }

    public class CreateAppUser
    {
        public AppUser appUser { get; set; }
        public string code { get; set; }
        public string message { get; set; }
    }


    public class CreateLogin
    {
        public int EmpId { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string Password { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public int IsActive { get; set; }
        public List<SelectListItem> RoleList { get; set; }
        public List<SelectListItem> EmployeeList { get; set; }
        public List<CreateLogin> LoginList { get; set; }
    }

    public class CreateLoginData
    {
        public string rolecode { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string creationdate { get; set; }
        public int roleid { get; set; }
        public string rolename { get; set; }
        public int isactive { get; set; }
        public int employid { get; set; }
        public string username { get; set; }
        public string lastname { get; set; }
        public int userid { get; set; }
    }

    public class CreateLoginDetails
    {
        public string code { get; set; }
        public List<CreateLoginData> data { get; set; }
        public string message { get; set; }
    }
    #endregion
}