﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lms_system.Models
{
    public class EmployeeModel
    {
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string PanNumber { get; set; }
        public string EmailId { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string DesignationCode { get; set; }
        public string DesignationName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int IsActive { get; set; }
        public List<EmployeeModel> EmployeeList { get; set; }
        public List<SelectListItem> RoleList { get; set; }
        public List<SelectListItem> BranchList { get; set; }
        public List<SelectListItem> DesignationList { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class EmployeeJsonDataDetail
    {
        public string firstname { get; set; }
        public string mobilenumber { get; set; }
        public string role { get; set; }
        public string createddate { get; set; }
        public string middlename { get; set; }
        public string emailid { get; set; }
        public string designation { get; set; }
        public string branch { get; set; }
        public string pannumber { get; set; }
        public string lastname { get; set; }
        public string employeeid { get; set; }
    }
    public class EmployeeJsonDetail
    {
        public string code { get; set; }
        public List<EmployeeJsonDataDetail> data { get; set; }
        public string message { get; set; }
    }

    public class NonAppUserEmployeesData
    {
        public int empid { get; set; }
        public string middlename { get; set; }
        public string firstname { get; set; }
        public string employeeid { get; set; }
        public string lastname { get; set; }
    }

    public class NonAppUserEmployees
    {
        public string code { get; set; }
        public List<NonAppUserEmployeesData> data { get; set; }
        public string message { get; set; }
    }
}