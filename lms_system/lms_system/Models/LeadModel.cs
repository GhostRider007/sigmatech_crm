﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lms_system.Models
{
    public class CreateLead
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string InterestedIn { get; set; }
        public string LeadFrom { get; set; }
        public string Remark { get; set; }
        public string ProductCode { get; set; }
        public string IsActive { get; set; }
        public List<SelectListItem> ProductList { get; set; }
    }

    public class ProductData
    {
        public int id { get; set; }
        public string creationdate { get; set; }
        public int isactive { get; set; }
        public string productcode { get; set; }
        public string productdesc { get; set; }
        public string productname { get; set; }
        public double wholesaleprice { get; set; }
    }

    public class Product
    {
        public string code { get; set; }
        public List<ProductData> data { get; set; }
        public string message { get; set; }
    }

    public class CreateProductMessage
    {
        public int id { get; set; }
        public string creationdate { get; set; }
        public int isactive { get; set; }
        public string productcode { get; set; }
        public string productdesc { get; set; }
        public string productname { get; set; }
        public double wholesaleprice { get; set; }
    }

    public class CreateProduct
    {
        public string code { get; set; }
        public CreateProductMessage message { get; set; }
    }

    public class LeadMessage
    {
        public int leadid { get; set; }
        public string country { get; set; }
        public string createdby { get; set; }
        public string creationdate { get; set; }
        public string emailid { get; set; }
        public string firstname { get; set; }
        public int isactive { get; set; }
        public string lastname { get; set; }
        public string leadsource { get; set; }
        public string mobile { get; set; }
        public string phone { get; set; }
        public string productcode { get; set; }
        public string remarks { get; set; }
        public string state { get; set; }
    }

    public class CreateLeadResponse
    {
        public string code { get; set; }
        public LeadMessage message { get; set; }
    }

    public class LeadListData
    {
        public int leadid { get; set; }
        public string country { get; set; }
        public string createdby { get; set; }
        public string creationdate { get; set; }
        public string emailid { get; set; }
        public string firstname { get; set; }
        public int isactive { get; set; }
        public string lastname { get; set; }
        public string leadsource { get; set; }
        public string mobile { get; set; }
        public string phone { get; set; }
        public string productcode { get; set; }
        public string remarks { get; set; }
        public string state { get; set; }
    }

    public class LeadListResponse
    {
        public string code { get; set; }
        public List<LeadListData> data { get; set; }
        public string message { get; set; }
    }
}