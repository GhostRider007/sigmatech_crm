﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace lms_system.Models.Common
{
    public static class Config
    {
        public static string WebsiteName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitename"]); }
        }

        public static string WebsiteUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websiteurl"]); }
        }

        public static string AdminEmail
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminemail"]); }
        }
        public static string SeeInsuredConString
        {
            get { return ConfigurationManager.ConnectionStrings["SeeInsured"].ConnectionString; }
        }
        public static string SeeInsuredApiConString
        {
            get { return ConfigurationManager.ConnectionStrings["SeeInsuredApi"].ConnectionString; }
        }
    }
}