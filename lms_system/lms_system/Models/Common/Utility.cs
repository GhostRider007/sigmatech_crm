﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace lms_system.Models.Common
{
    public static class Utility
    {
        public static string GenrateRandomId(string prefixString, int SizeLimit, string charCombo = null)
        {
            try
            {
                char[] chars = !string.IsNullOrEmpty(charCombo) ? charCombo.ToCharArray() : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[SizeLimit];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetBytes(data);
                }

                StringBuilder result = new StringBuilder(SizeLimit);

                if (!string.IsNullOrWhiteSpace(prefixString))
                {
                    result.Append(prefixString);
                }

                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static string GetDescription(string description, int? length = null)
        {
            string publicDescription = string.Empty;

            if (!string.IsNullOrEmpty(description))
            {
                publicDescription = description;

                publicDescription = publicDescription.Replace("\r\n", "<br/>").Replace("\n", "<br/>").Replace("amp;", "  ");

                if (length != null && publicDescription.Length > length)
                {
                    publicDescription = publicDescription.Remove((int)length);

                    if (publicDescription.LastIndexOf(" ") > 0)
                    {
                        publicDescription = publicDescription.Remove(publicDescription.LastIndexOf(" "));
                    }
                }
            }

            return publicDescription;
        }
        public static string ReadHTMLTemplate(string fileName)
        {
            using (StreamReader streamReader = new StreamReader(HttpContext.Current.Server.MapPath("~") + "\\" + "/EmailTemplates" + "/" + fileName))
            {
                try
                {
                    string xmlString = streamReader.ReadToEnd();
                    return xmlString;
                }
                finally
                {
                    streamReader.Close();
                    streamReader.Dispose();
                }
            }
        }
        public static string ConvertStringDateToStringDateFormate(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("dd MMM yyyy hh:mm tt");
            }

            return string.Empty;
        }
    }
}