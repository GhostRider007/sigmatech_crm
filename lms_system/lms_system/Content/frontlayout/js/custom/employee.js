﻿$('#AddEditEmpModal').on('hidden.bs.modal', function () {
    window.location.reload();
});
$('#EmployeeModal').on('hidden.bs.modal', function () {
    window.location.reload();
});

var employee = {};
function CheckEmployeeForm() {
    $("#EmployeeError").html("");
    $("#EmployeeFooter").html("");
    $("#empmodalfooter").css("display", "none");

    if (CheckFocusBlankValidation("txtFirstName")) return !1;
    if (CheckFocusBlankValidation("txtLastName")) return !1;
    if (CheckFocusBlankValidation("txtMobileNumber")) return !1;
    if (CheckFocusBlankValidation("txtPanNumber")) return !1;
    if (CheckFocusBlankValidation("txtEmailId")) return !1;
    if (CheckEmailValidatoin("txtEmailId")) return !1;
    if (CheckFocusBlankValidation("RoleCode")) return !1;
    //if (CheckFocusDropDownBlankValidation("BranchCode")) return !1;
    //if (CheckFocusDropDownBlankValidation("DesignationCode")) return !1;

    employee.EmployeeId = $("#hdnEmpId").val();
    employee.FirstName = $("#txtFirstName").val();
    employee.MiddleName = $("#txtMiddleName").val();
    employee.LastName = $("#txtLastName").val();
    employee.MobileNumber = $("#txtMobileNumber").val();
    employee.PanNumber = $("#txtPanNumber").val();
    employee.EmailId = $("#txtEmailId").val();
    employee.RoleCode = $("#RoleCode option:selected").val();
    employee.BranchCode = $("#BranchCode option:selected").val();
    employee.DesignationCode = $("#DesignationCode option:selected").val();

    $("#btnEmpSubmit").attr("disabled", true).html("Processing...<i class='fa fa-spinner fa-pulse' aria-hidden='true'></i>");
    $.ajax({
        type: "Post",
        url: "/Employee/ProcessToInsertEmployee",
        data: '{model: ' + JSON.stringify(employee) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#btnEmpSubmit").attr("disabled", false).html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
            if (data != null) {
                if (data[0] == "true") {
                    ResetEmpForm();
                    $("#EmployeeFormBody").html("<h5 class='text-success text-center'>" + data[1] + "</h5>");
                    //$("#EmployeeFooter").html("<button type='button' style='cursor: pointer;' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-close' aria-hidden='true'></i>&nbsp;Close</button>");
                    $("#empmodalfooter").css("display", "block");
                }
                else if (data[0] == "logout") {
                    window.location.reload();
                }
                else {
                    $("#EmployeeError").html("<h6>" + data[1] + "</h6>");
                }
            }
        }
    });
}

function EmployeeAddEdit(headingid, bodyid, footerid, message, action) {
    if (action == "success") {
        $("#" + headingid).css("color", "#28a745").html("<i class='fa fa-check-circle text-success'></i>&nbsp;Success");
        $("#" + bodyid).html("<h5 class='text-success text-center'>" + message + "</h5>");
    }
    else {
        $("#" + headingid).css("color", "#dc3545").html("<i class='fa fa-exclamation-triangle text-danger'></i>&nbsp;Failed");
        $("#" + bodyid).html("<h5 class='text-danger text-center'>" + message + "</h5>");
    }
    $("#" + footerid).html("<button type='button' style='cursor: pointer;' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-close' aria-hidden='true'></i>&nbsp;Close</button>");
    //$("#empmodalfooter").prop("display", true);
}

$(document.body).on('click', ".employeeredirect", function (e) {
    $(this).html("Wait...<i class='fa fa-spinner fa-pulse' aria-hidden='true'></i>");
    window.location.href = "/employee/employeelist";
});

function PopupMsg(headingcolor, heading, headbefoicon, msg, msgcolor, closebtncolor, addattr) {
    $(".modelheading").css("color", headingcolor); $(".spanmsg").html(heading); $("#iconid").addClass(headbefoicon); $(".sucessmsg").css("color", msgcolor).html(msg);
    $("#btnPopupClose").css("background", closebtncolor);
    if (addattr == "failed") {
        $("#btnPopupClose").attr("data-dismiss", "modal");
    }
    $(".successmessage").click();
}

function ResetEmpForm() {
    $("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtMiddleName").val("");
    $("#txtMobileNumber").val("");
    $("#txtPanNumber").val("");
    $("#txtEmailId").val("");
}

function ModifyEmployeeDetail(firstname, middlename, lastname, mobile, pan, email, rolecode, branchcode, degicode, empid) {
    $("#EmployeeError").html("");
    $("#EmployeeFooter").html("");
    $("#empmodalfooter").css("display", "none");

    $("#hdnEmpId").val(empid);
    $("#txtFirstName").val(firstname);
    $("#txtLastName").val(middlename);
    $("#txtMiddleName").val(lastname);
    $("#txtMobileNumber").val(mobile);
    $("#txtPanNumber").val(pan).prop("disabled", true);
    $("#txtEmailId").val(email);
    $("#RoleCode").val(rolecode).trigger('change');
    $("#BranchCode").val(branchcode).trigger('change');
    $("#DesignationCode").val(degicode).trigger('change');
    $(".addeditemployeemodal").click();
}

//$(".addeditemployeemodal").click(function () {
//    $("#EmployeeFooter").html("<button type='button' style='cursor: pointer;' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-close' aria-hidden='true'></i>&nbsp;Close</button>");
//});

function DeleteEmployeeDetail(empcode) {
    if (empcode != "") {
        if (confirm("Are you sure you want to delete this entry?")) {
            $("#employeedelete_" + empcode).removeClass("fa-trash").addClass("fa-pulse fa-spinner");
            $.ajax({
                type: "Post",
                url: "/Employee/ProcessToDeleteEmployee",
                data: '{code: ' + JSON.stringify(empcode) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        if (data[0] == "true") {
                            DeletePopupMessage("exampleModalLabel", "EmployeeBody", "EmployeeFooter", data[1], "employeemodal", "success");
                        }
                        else if (data[0] == "logout") {
                            window.location.reload();
                        }
                        else {
                            $("#employeedelete_" + empcode).removeClass("fa-pulse fa-spinner").addClass("fa-trash");
                            DeletePopupMessage("exampleModalLabel", "EmployeeBody", "EmployeeFooter", data[1], "employeemodal", "failed");
                        }
                    }
                }
            });
        }
    }
}

function RestoreEmployeeDetail(empcode) {
    if (empcode != "") {
        if (confirm("Are you sure you want to restore this entry?")) {
            var model = {};
            model.EmployeeId = empcode;
            model.IsActive = 1;
            $("#employeerestore_" + empcode).removeClass("fa-undo").addClass("fa-pulse fa-spinner");
            $.ajax({
                type: "Post",
                url: "/Employee/ProcessToInsertEmployee",
                data: '{model: ' + JSON.stringify(model) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    window.location.reload();
                }
            });
        }
    }
}

function DeletePopupMessage(headingid, bodyid, footerid, message, classclick, action) {
    if (action == "success") {
        $("#" + headingid).css("color", "#28a745").html("<i class='fa fa-check-circle text-success'></i>&nbsp;Success");
        $("#" + bodyid).html("<h5 class='text-success text-center'>" + message + "</h5>");
    }
    else {
        $("#" + headingid).css("color", "#dc3545").html("<i class='fa fa-exclamation-triangle text-danger'></i>&nbsp;Failed");
        $("#" + bodyid).html("<h5 class='text-danger text-center'>" + message + "</h5>");
    }
    $("#" + footerid).html("<button type='button' style='cursor: pointer;' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-close' aria-hidden='true'></i>&nbsp;Close</button>");
    $("." + classclick).click();
}

//========================Dropdown with search area==================================
$("#RoleCode").select2();
$("#BranchCode").select2();
$("#DesignationCode").select2();