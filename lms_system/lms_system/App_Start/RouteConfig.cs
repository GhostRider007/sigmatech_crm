﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace lms_system
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "AdminDashboard", url: "dashboard", defaults: new { controller = "Dashboard", action = "AdminDashboard" });
            routes.MapRoute(name: "EmployeeList", url: "sigmatech/employee-master", defaults: new { controller = "Employee", action = "EmployeeList" });
            routes.MapRoute(name: "EmployeeAddEdit", url: "employee-add-edit", defaults: new { controller = "Employee", action = "AddEditEmployee" });

            routes.MapRoute(name: "BranchMaster", url: "sigmatech/branch-master", defaults: new { controller = "Privilege", action = "BranchList" });
            routes.MapRoute(name: "DesignationMaster", url: "sigmatech/designation-master", defaults: new { controller = "Privilege", action = "DesignationList" });
            routes.MapRoute(name: "ProductMaster", url: "sigmatech/enroll-product", defaults: new { controller = "LeadMaster", action = "ProductMaster" });
            //routes.MapRoute(name: "LeadMaster", url: "sigmatech/lead-master", defaults: new { controller = "LeadMaster", action = "LeadMaster" });
            routes.MapRoute(name: "LeadMaster", url: "sigmatech/lead-entry", defaults: new { controller = "LeadMaster", action = "LeadMaster" });
            routes.MapRoute(name: "AddEditLeadMaster", url: "sigmatech/add-edit-lead-entry", defaults: new { controller = "LeadMaster", action = "AddEditLeadMaster" });

            routes.MapRoute(name: "RoleMaster", url: "sigmatech/role-master", defaults: new { controller = "Privilege", action = "RoleList" });
            routes.MapRoute(name: "RolePermission", url: "sigmatech/role-permission", defaults: new { controller = "Privilege", action = "RolePermission" });
            routes.MapRoute(name: "CreateLogin", url: "sigmatech/create-app-user", defaults: new { controller = "Privilege", action = "AssignRoleList" });

            routes.MapRoute(name: "LogoutUser", url: "logout-user", defaults: new { controller = "Dashboard", action = "LogoutUser" });
            routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}", defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}
