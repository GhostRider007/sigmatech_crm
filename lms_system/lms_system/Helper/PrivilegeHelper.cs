﻿using lms_system.APIHelper;
using lms_system.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using lms_system.Models.Common;

namespace lms_system.Helper
{
    public static class PrivilegeHelper
    {
        #region [Branch Section]
        public static List<BranchModel> GetAllActiveBranchList()
        {
            List<BranchModel> branchList = new List<BranchModel>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetAllActiveBranchList(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        BranchDetails branchDetail = JsonConvert.DeserializeObject<BranchDetails>(response);
                        if (branchDetail.code == "success")
                        {
                            foreach (var item in branchDetail.data)
                            {
                                BranchModel branch = new BranchModel();
                                branch.BranchId = item.branchid;
                                branch.BranchCode = item.branchcode;
                                branch.BranchName = item.branchname;
                                branch.BranchContactNumber = item.branchcontactno;
                                branch.BranchAddress = item.branchaddress;
                                branch.CreatedDate = Utility.ConvertStringDateToStringDateFormate(item.createddate.ToString());
                                branch.IsActive = item.isactive;
                                branchList.Add(branch);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return branchList;
        }
        public static List<string> InsertBranchDetail(BranchModel branch)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string reqPara = "{\"branchaddress\": \"" + branch.BranchAddress + "\",\"branchcode\": \"" + branch.BranchCode + "\",\"branchcontactno\": \"" + branch.BranchContactNumber + "\",\"branchname\": \"" + branch.BranchName + "\",\"isactive\": \"" + branch.IsActive + "\"}";

                    string response = LMSProductAPI.InsertBranchDetail(reqPara, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        if (response == "logout")
                        {
                            result.Add("logout");
                        }
                        else
                        {
                            dynamic obj = JObject.Parse(response);
                            string status = obj.status;
                            string respoMsg = obj.message;

                            if (status == "success")
                            {
                                result.Add("true");
                                result.Add(respoMsg);
                            }
                        }
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }

            return result;
        }
        public static List<string> ProcessToDeleteBranch(string code)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.DeleteBranchDetail(code, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        if (response == "logout")
                        {
                            result.Add("logout");
                        }
                        else
                        {
                            dynamic obj = JObject.Parse(response);
                            string status = obj.status;
                            string respoMsg = obj.message;

                            if (status == "success")
                            {
                                result.Add("true");
                                result.Add(respoMsg);
                            }
                            else
                            {
                                result.Add("false");
                                result.Add(respoMsg);
                            }
                        }
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }

            return result;
        }
        #endregion

        #region [Role Section]
        public static List<RoleModel> GetAllActiveRoleList()
        {
            List<RoleModel> roleList = new List<RoleModel>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetAllActiveRoleList(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        RoleDetails roleDetail = JsonConvert.DeserializeObject<RoleDetails>(response);
                        if (roleDetail.code == "success")
                        {
                            foreach (var item in roleDetail.data)
                            {
                                RoleModel role = new RoleModel();
                                role.RoleId = item.roleid;
                                role.RoleCode = item.rolecode;
                                role.RoleName = item.rolename;
                                role.RoleDescription = item.roledescription;
                                role.CreatedDate = Utility.ConvertStringDateToStringDateFormate(item.entrydate);
                                role.IsActive = item.isactive;
                                roleList.Add(role);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return roleList;
        }
        public static List<string> InsertRoleDetail(RoleModel role)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string reqPara = "{\"rolecode\":\"" + role.RoleCode + "\",\"rolename\":\"" + role.RoleName + "\",\"roledescription\":\"" + role.RoleDescription + "\",\"isactive\":" + role.IsActive + "}";

                    string response = LMSProductAPI.InsertRoleDetail(reqPara, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        if (response == "logout")
                        {
                            result.Add("logout");
                        }
                        else
                        {
                            dynamic obj = JObject.Parse(response);
                            string status = obj.status;
                            string respoMsg = obj.message;

                            if (status == "success")
                            {
                                result.Add("true");
                                result.Add(respoMsg);
                            }
                        }
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }

            return result;
        }
        public static List<string> ProcessToDeleteRole(string code)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.DeleteRoleDetail(code, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        if (response == "logout")
                        {
                            result.Add("logout");
                        }
                        else
                        {
                            dynamic obj = JObject.Parse(response);
                            string status = obj.status;
                            string respoMsg = obj.message;

                            if (status == "success")
                            {
                                result.Add("true");
                                result.Add(respoMsg);
                            }
                            else
                            {
                                result.Add("false");
                                result.Add(respoMsg);
                            }
                        }
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }

            return result;
        }
        public static List<RolePermissionRights> GetRolePermissionsByRoleId(string roleid, ref string msg)
        {
            List<RolePermissionRights> roleRights = new List<RolePermissionRights>();
            try
            {
                if (!string.IsNullOrEmpty(roleid))
                {
                    string access_token = string.Empty;
                    if (AccountHelper.UserLoginExist(ref access_token))
                    {
                        string response = LMSProductAPI.GetRolePermissionsByRoleId(roleid, access_token, ref msg);
                        if (!string.IsNullOrEmpty(response))
                        {
                            RolePermissionByRole roleRightsDetails = JsonConvert.DeserializeObject<RolePermissionByRole>(response);
                            if (roleRightsDetails.code == "success")
                            {
                                roleRights = roleRightsDetails.data;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return roleRights;
        }
        public static string UpdateRolePermission(string reqstr)
        {
            string result = string.Empty;
            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.UpdateRolePermission(access_token, reqstr, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic obj = JObject.Parse(response);
                        string status = obj.code;
                        string message = obj.message;
                        if (status == "success")
                        {
                            result = message;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion

        #region [Designation Section]
        public static List<DesignationModel> GetAllActiveDesignationList()
        {
            List<DesignationModel> descList = new List<DesignationModel>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetAllActiveDesignationList(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        DesignationDetails descDetail = JsonConvert.DeserializeObject<DesignationDetails>(response);
                        if (descDetail.code == "success")
                        {
                            foreach (var item in descDetail.data)
                            {
                                DesignationModel desc = new DesignationModel();
                                desc.DesignationId = item.designationid;
                                desc.DesignationCode = item.desigcode;
                                desc.DesignationName = item.designame;
                                desc.Description = item.desigdesc;
                                desc.CreatedDate = Utility.ConvertStringDateToStringDateFormate(item.createddate);
                                desc.IsActive = item.isactive;
                                descList.Add(desc);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return descList;
        }
        public static List<string> InsertDesignationDetail(DesignationModel designation)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string reqPara = "{\"desigcode\":\"" + designation.DesignationCode + "\",\"designame\":\"" + designation.DesignationName + "\",\"desigdesc\":\"" + designation.Description + "\",\"isactive\":" + designation.IsActive + "}";

                    string response = LMSProductAPI.InsertDesignationDetail(reqPara, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        if (response == "logout")
                        {
                            result.Add("logout");
                        }
                        else
                        {
                            dynamic obj = JObject.Parse(response);
                            string status = obj.status;
                            string respoMsg = obj.message;

                            if (status == "success")
                            {
                                result.Add("true");
                                result.Add(respoMsg);
                            }
                        }
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }

            return result;
        }
        public static List<string> ProcessToDeleteDesignation(string code)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.DeleteDesignationDetail(code, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        if (response == "logout")
                        {
                            result.Add("logout");
                        }
                        else
                        {
                            dynamic obj = JObject.Parse(response);
                            string status = obj.status;
                            string respoMsg = obj.message;

                            if (status == "success")
                            {
                                result.Add("true");
                                result.Add(respoMsg);
                            }
                            else
                            {
                                result.Add("false");
                                result.Add(respoMsg);
                            }
                        }
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }

            return result;
        }
        #endregion
        public static List<RoleModel> GetRoleForDropdown()
        {
            List<RoleModel> roleList = new List<RoleModel>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetRoleForDropdown(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        RoleDropDown roleDropDownList = JsonConvert.DeserializeObject<RoleDropDown>(response);
                        if (roleDropDownList.code == "success")
                        {
                            foreach (var item in roleDropDownList.data)
                            {
                                RoleModel role = new RoleModel();
                                role.RoleCode = item.rolecode;
                                role.RoleName = item.rolename;
                                role.RoleId = item.roleid;
                                roleList.Add(role);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return roleList;
        }
        public static List<BranchModel> GetBranchForDropdown()
        {
            List<BranchModel> branchList = new List<BranchModel>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetBranchForDropdown(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        BranchDropDown branchDropDownList = JsonConvert.DeserializeObject<BranchDropDown>(response);
                        if (branchDropDownList.code == "success")
                        {
                            foreach (var item in branchDropDownList.data)
                            {
                                BranchModel branch = new BranchModel();
                                branch.BranchCode = item.branchcode;
                                branch.BranchName = item.branchname;
                                branchList.Add(branch);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return branchList;
        }
        public static List<DesignationModel> GetDesignationForDropdown()
        {
            List<DesignationModel> degList = new List<DesignationModel>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetDesignationForDropdown(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        DesignationDropDown roleDropDownList = JsonConvert.DeserializeObject<DesignationDropDown>(response);
                        if (roleDropDownList.code == "success")
                        {
                            foreach (var item in roleDropDownList.data)
                            {
                                DesignationModel deg = new DesignationModel();
                                deg.DesignationCode = item.desigcode;
                                deg.DesignationName = item.designame;
                                degList.Add(deg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return degList;
        }
        public static CreateAppUser InsertCreateLoginDetail(CreateLogin model)
        {
            CreateAppUser result = new CreateAppUser();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string reqPara = "{\"username\":\"" + model.EmployeeCode + "\",\"employid\":" + model.EmpId + ",\"roleid\":" + model.RoleId + ",\"password\":\"" + model.Password + "\",\"isactive\":" + model.IsActive + "}";

                    string response = LMSProductAPI.InsertCreateLoginDetail(reqPara, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        result = JsonConvert.DeserializeObject<CreateAppUser>(response);
                    }
                    //if (!string.IsNullOrEmpty(response))
                    //{
                    //    if (response == "logout")
                    //    {
                    //        result.Add("logout");
                    //    }
                    //    else
                    //    {
                    //        dynamic obj = JObject.Parse(response);
                    //        //string status = obj.status;
                    //        string respoMsg = obj.message;

                    //        //if (status == "success")
                    //        //{
                    //        result.Add("true");
                    //        result.Add(respoMsg);
                    //        //}
                    //    }
                    //}
                    //else
                    //{
                    //    result.Add("false");
                    //    result.Add(msg);
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static CreateLoginDetails GetCreateLoginDetails()
        {
            CreateLoginDetails loginDetail = new CreateLoginDetails();
            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetCreateLoginDetails(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        loginDetail = JsonConvert.DeserializeObject<CreateLoginDetails>(response);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return loginDetail;
        }
    }
}