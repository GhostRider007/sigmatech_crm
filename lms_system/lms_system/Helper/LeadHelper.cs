﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using lms_system.APIHelper;
using lms_system.Models;

namespace lms_system.Helper
{
    public static class LeadHelper
    {
        public static List<ProductData> GetProductList()
        {
            List<ProductData> productList = new List<ProductData>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetProductList(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        Product productDetail = JsonConvert.DeserializeObject<Product>(response);
                        if (productDetail.code == "success")
                        {
                            productList = productDetail.data;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return productList;
        }

        public static bool InsertProductDetail(string pname, string pcode, string pdesc, string pwsrate, ref int id)
        {
            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string pPara = "{\"productcode\":\"" + pcode + "\",\"productdesc\":\"" + pdesc + "\",\"productname\":\"" + pname + "\",\"wholesaleprice\":" + pwsrate + ",\"isactive\":1}";
                    string response = LMSProductAPI.InsertProductDetail(pPara, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        CreateProduct product = JsonConvert.DeserializeObject<CreateProduct>(response);
                        if (product.code == "success")
                        {
                            id = product.message.id;
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool InsertLeadDetail(CreateLead model, ref int id, ref string msg)
        {
            try
            {
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string pPara = "{\"country\":\"" + model.Country + "\", \"emailid\":\"" + model.EmailId + "\",\"firstname\":\"" + model.FirstName + "\",\"isactive\": 1,\"lastname\":\"" + model.LastName + "\",\"leadsource\":\"" + model.LeadFrom + "\",\"mobile\":\"" + model.MobileNumber + "\",\"phone\":\"" + model.PhoneNumber + "\",\"productcode\":\"" + model.ProductCode + "\",\"remarks\":\"" + model.Remark + "\",\"state\":\"" + model.State + "\"}";
                    string response = LMSProductAPI.InsertLeadDetail(pPara, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        CreateLeadResponse lead = JsonConvert.DeserializeObject<CreateLeadResponse>(response);
                        if (lead.code == "success")
                        {
                            id = lead.message.leadid;
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static LeadListResponse GetLeadListResponse()
        {
            LeadListResponse leadResponse = new LeadListResponse();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetLeadList(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        leadResponse = JsonConvert.DeserializeObject<LeadListResponse>(response);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return leadResponse;
        }
    }
}