﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lms_system.APIHelper;
using lms_system.Models;
using lms_system.Models.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace lms_system.Helper
{
    public static class EmployeeHelper
    {
        public static List<string> InsertUpdateEmployeeDetail(EmployeeModel emp)
        {
            List<string> result = new List<string>();
            try
            {
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string msg = string.Empty;

                    string empPara = "{\"firstName\":\"" + emp.FirstName + "\",\"middleName\":\"" + emp.MiddleName + "\",\"lastName\":\"" + emp.LastName + "\",\"mobileNumber\":\"" + emp.MobileNumber + "\",\"panNumber\":\"" + emp.PanNumber + "\",\"emailid\":\"" + emp.EmailId + "\",\"rolecode\":\"" + emp.RoleCode + "\",\"branchcode\":\"" + emp.BranchCode + "\",\"designationcode\":\"" + emp.DesignationCode + "\"";

                    if (!string.IsNullOrEmpty(emp.EmployeeId))
                    {
                        empPara = empPara + ",\"employeeID\":\"" + emp.EmployeeId + "\"";
                    }
                    empPara = empPara + "}";

                    string response = LMSProductAPI.InsertUpdateEmployeeDetail(empPara, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic obj = JObject.Parse(response);
                        string status = obj.status;
                        string message = obj.message;
                        if (status == "success")
                        {
                            result.Add("true");
                            result.Add(message);
                        }
                        else
                        {
                            result.Add("false");
                            result.Add(message);
                        }
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
                else
                {
                    AccountHelper.LogoutUser("inner");
                    result.Add("logout");
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }

            return result;
        }
        public static List<EmployeeModel> GetAllActiveEmployeeList()
        {
            List<EmployeeModel> empList = new List<EmployeeModel>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetAllActiveEmployeeList(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        EmployeeJsonDetail branchDetail = JsonConvert.DeserializeObject<EmployeeJsonDetail>(response);
                        if (branchDetail.code == "success")
                        {
                            foreach (var item in branchDetail.data)
                            {
                                EmployeeModel emp = new EmployeeModel();
                                emp.FirstName = item.firstname;
                                emp.EmployeeId = item.employeeid;
                                emp.MiddleName = item.middlename;
                                emp.LastName = item.lastname;
                                emp.MobileNumber = item.mobilenumber;
                                emp.PanNumber = item.pannumber;
                                emp.EmailId = item.emailid;
                                string rolesec = item.role;
                                if (!string.IsNullOrEmpty(rolesec))
                                {
                                    emp.RoleCode = rolesec.Split(',')[0];
                                    emp.RoleName = rolesec.Split(',')[1];
                                }
                                string branchsec = item.branch;
                                if (!string.IsNullOrEmpty(branchsec))
                                {
                                    emp.BranchCode = branchsec.Split(',')[0];
                                    emp.BranchName = branchsec.Split(',')[1];
                                }
                                string degsec = item.designation;
                                if (!string.IsNullOrEmpty(degsec))
                                {
                                    emp.DesignationCode = degsec.Split(',')[0];
                                    emp.DesignationName = degsec.Split(',')[1];
                                }
                                emp.CreatedDate = item.createddate != null ? Utility.ConvertStringDateToStringDateFormate(item.createddate.ToString()) : string.Empty;
                                emp.IsActive = 1;
                                empList.Add(emp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return empList;
        }

        public static List<NonAppUserEmployeesData> GetNonAppUserEmployeesList()
        {
            List<NonAppUserEmployeesData> nonEmpList = new List<NonAppUserEmployeesData>();
            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.GetNonAppUserEmployeesList(access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        NonAppUserEmployees branchDetail = JsonConvert.DeserializeObject<NonAppUserEmployees>(response);
                        if (branchDetail.code == "success")
                        {
                            foreach (var item in branchDetail.data)
                            {
                                NonAppUserEmployeesData nonEmp = new NonAppUserEmployeesData();

                                nonEmp.empid = item.empid;
                                nonEmp.middlename = item.middlename;
                                nonEmp.firstname = item.firstname;
                                nonEmp.employeeid = item.employeeid;
                                nonEmp.lastname = item.lastname;

                                nonEmpList.Add(nonEmp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return nonEmpList;
        }
        public static List<string> ProcessToDeleteEmployee(string code)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string access_token = string.Empty;
                if (AccountHelper.UserLoginExist(ref access_token))
                {
                    string response = LMSProductAPI.DeleteEmployeeDetail(code, access_token, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        if (response == "logout")
                        {
                            result.Add("logout");
                        }
                        else
                        {
                            dynamic obj = JObject.Parse(response);
                            string status = obj.status;
                            string respoMsg = obj.message;

                            if (status == "success")
                            {
                                result.Add("true");
                                result.Add(respoMsg);
                            }
                            else
                            {
                                result.Add("false");
                                result.Add(respoMsg);
                            }
                        }
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }

            return result;
        }
    }
}