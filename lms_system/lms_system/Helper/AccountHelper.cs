﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using lms_system.APIHelper;
using lms_system.Models;

namespace lms_system.Helper
{
    public static class AccountHelper
    {
        public static bool BackOfficeLogin(BackOfficeLogin login, ref string msg)
        {
            bool isLogin = false;

            try
            {
                string access_token = string.Empty;
                if (UserLoginExist(ref access_token))
                {
                    return true;
                }
                else
                {
                    string apiResponse = LMSProductAPI.GetAuthenticate(login.UserId, login.Password, ref msg);
                    if (apiResponse != null)
                    {
                        UserAccountDetails userDetail = JsonConvert.DeserializeObject<UserAccountDetails>(apiResponse);

                        if (!string.IsNullOrEmpty(userDetail.webtoken))
                        {
                            InitializeAccessTokenSession(userDetail);
                            isLogin = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                msg = "Error: Network not available!";
            }

            return isLogin;
        }
        public static bool IsUserLoggedIn()
        {
            string access_token = string.Empty;
            return UserLoginExist(ref access_token);
        }
        public static void LogoutUser(string actionType = null)
        {
            HttpContext.Current.Session.Remove("accessToken");
            HttpContext.Current.Session.Remove("UserDetail");
            if (actionType == "outer")
            {
                HttpContext.Current.Response.Redirect("/", true);
            }
        }
        public static void InitializeAccessTokenSession(UserAccountDetails userDetail)
        {
            if (!string.IsNullOrEmpty(userDetail.webtoken))
            {
                List<string> objAccessToken = new List<string>();
                objAccessToken.Add(userDetail.webtoken);
                objAccessToken.Add(DateTime.Now.ToString());
                objAccessToken.Add("exist");
                HttpContext.Current.Session["AccessToken"] = objAccessToken;
                HttpContext.Current.Session["UserDetail"] = userDetail;
            }
        }
        public static bool UserLoginExist(ref string token)
        {
            if (HttpContext.Current.Session["UserDetail"] != null)
            {
                List<string> objAccessToken = (List<string>)HttpContext.Current.Session["AccessToken"];
                TimeSpan TS = DateTime.Now - Convert.ToDateTime(objAccessToken[1]);
                if (TS.Minutes < 1440)
                {
                    token = objAccessToken[0];
                    return true;
                }
                else
                {
                    HttpContext.Current.Session["AccessToken"] = null;
                    HttpContext.Current.Session["UserDetail"] = null;
                }
            }

            return false;
        }
        public static UserAccountDetails GetCurrentUserDetails()
        {
            UserAccountDetails userDel = new UserAccountDetails();

            if (HttpContext.Current.Session["UserDetail"] != null)
            {
                userDel = (UserAccountDetails)HttpContext.Current.Session["UserDetail"];
            }

            return userDel;
        }

        //public static bool BackOfficeLogin(BackOfficeLogin login, ref string msg)
        //{
        //    bool isLogin = false;

        //    try
        //    {
        //        string token_key = LMSProductAPI.GetAuthenticate(login.UserId, login.Password, ref msg);
        //        if (!string.IsNullOrEmpty(token_key))
        //        {
        //            isLogin = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        msg = "Error: Network not available!";
        //    }

        //    return isLogin;
        //}

        //public static bool IsUserLoggedIn()
        //{
        //    string access_token = string.Empty;
        //    return LMSProductAPI.IsTokenExist(ref access_token);
        //}

        //public static void LogoutUser(string actionType = null)
        //{
        //    HttpContext.Current.Session.Remove("accessToken");
        //    if (actionType == "outer")
        //    {
        //        HttpContext.Current.Response.Redirect("/", true);
        //    }
        //}
    }
}