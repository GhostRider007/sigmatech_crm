﻿$('#BranchModal').on('hidden.bs.modal', function () {
    window.location.reload();
});

//================Branch Section===============
function BranchSection() {
    SuccessFailMessage("", "", "", "BranchError", "", "");
    var model = {};
    if (CheckFocusBlankValidation("txtBranchCode")) return !1;
    if (CheckFocusBlankValidation("txtBranchName")) return !1;
    if (CheckFocusBlankValidation("txtBranchContNo")) return !1;
    if (CheckFocusBlankValidation("txtBranchAddress")) return !1;

    model.BranchId = 0;
    model.BranchCode = $("#txtBranchCode").val();
    model.BranchName = $("#txtBranchName").val();
    model.BranchContactNumber = $("#txtBranchContNo").val();
    model.BranchAddress = $("#txtBranchAddress").val();
    model.IsActive = $('#chkBranchIsActive').is(':checked') == true ? 1 : 0;

    $("#btnBranchSubmit").attr("disabled", true).html("Processing...<i class='fa fa-spinner fa-pulse' aria-hidden='true'></i>");
    $.ajax({
        type: "Post",
        url: "/Privilege/ProcessToInsertUpdateBranch",
        data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#btnBranchSubmit").attr("disabled", false).html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
            if (data != null) {
                if (data[0] == "true") {
                    ResetBranchForm();
                    SuccessFailMessage("BranchBody", data[1], "BranchFooter", "", "", "success");
                }
                else if (data[0] == "logout") {
                    window.location.reload();
                }
                else {
                    SuccessFailMessage("", "", "", "BranchError", data[1], "failed");
                }
            }
        }
    });
}

function ResetBranchForm() {
    $("#txtBranchCode").val("");
    $("#txtBranchName").val("");
    $("#txtBranchContNo").val("");
    $("#txtBranchAddress").val("");
    if ($('#chkBranchIsActive').is(':checked') == false) {
        $(".switchery").click();
        $("#chkText").css("color", "rgb(38, 185, 154)").html("Active");
    }
}

function ModifyBranchDetail(code, name, contno, address, isactive) {
    $("#txtBranchCode").val(code).prop("disabled", true);
    $("#txtBranchName").val(name);
    $("#txtBranchContNo").val(contno);
    $("#txtBranchAddress").val(address);
    var isgreen = $(".switchery small").css("left");
    if (isactive == "1") {
        if (isgreen != "40px") {
            $("#chkText").css("color", "rgb(38, 185, 154)").html("Active");
            $(".switchery").click();
        }
    }
    else {
        if (isgreen != "0px") {
            $("#chkText").css("color", "#cc2929").html("InActive");
            $(".switchery").click();
        }
    }
    $("#exampleModalLabel").html("<i class='fa fa-plus' aria-hidden='true'></i>&nbsp;MODIFY BRANCH");
    $("#btnBranchSubmit").html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Modify");
    $(".branchmodal").click();
}

function DeleteBranchDetail(branchcode) {
    if (branchcode != "") {
        if (confirm("Are you sure you want to delete this entry?")) {
            $("#branchdelete_" + branchcode).removeClass("fa-trash").addClass("fa-pulse fa-spinner");
            $.ajax({
                type: "Post",
                url: "/Privilege/ProcessToDeleteBranch",
                data: '{code: ' + JSON.stringify(branchcode) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        if (data[0] == "true") {
                            DeletePopupMessage("exampleModalLabel", "BranchBody", "BranchFooter", data[1], "branchmodal", "success");
                        }
                        else if (data[0] == "logout") {
                            window.location.reload();
                        }
                        else {
                            $("#branchdelete_" + branchcode).removeClass("fa-pulse fa-spinner").addClass("fa-trash");
                            DeletePopupMessage("exampleModalLabel", "BranchBody", "BranchFooter", data[1], "branchmodal", "failed");
                        }
                    }
                }
            });
        }
    }
}

function RestoreBranchDetail(branchcode) {
    if (branchcode != "") {
        if (confirm("Are you sure you want to restore this entry?")) {
            var model = {};
            model.BranchCode = branchcode;
            model.IsActive = 1;
            $("#branchrestore_" + branchcode).removeClass("fa-undo").addClass("fa-pulse fa-spinner");
            $.ajax({
                type: "Post",
                url: "/Privilege/ProcessToInsertUpdateBranch",
                data: '{model: ' + JSON.stringify(model) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    window.location.reload();
                }
            });
        }
    }
}

//================Role Section===============

function RoleSection() {
    SuccessFailMessage("", "", "", "RoleError", "", "");
    var model = {};
    if (CheckFocusBlankValidation("txtRoleCode")) return !1;
    if (CheckFocusBlankValidation("txtRoleName")) return !1;
    if (CheckFocusBlankValidation("txtRoleDescription")) return !1;

    model.RoleId = 0;
    model.RoleCode = $("#txtRoleCode").val();
    model.RoleName = $("#txtRoleName").val();
    model.RoleDescription = $("#txtRoleDescription").val();
    model.IsActive = $('#chkRoleIsActive').is(':checked') == true ? 1 : 0;

    $("#btnRoleSubmit").attr("disabled", true).html("Processing...<i class='fa fa-spinner fa-pulse' aria-hidden='true'></i>");
    $.ajax({
        type: "Post",
        url: "/Privilege/ProcessToInsertUpdateRole",
        data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#btnRoleSubmit").attr("disabled", false).html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
            if (data != null) {
                if (data[0] == "true") {
                    ResetRoleForm();
                    SuccessFailMessage("RoleBody", data[1], "RoleFooter", "", "", "success");
                }
                else if (data[0] == "logout") {
                    window.location.reload();
                }
                else {
                    SuccessFailMessage("", "", "", "RoleError", data[1], "failed");
                }
            }
        }
    });
}

function ResetRoleForm() {
    $("#txtRoleCode").val("");
    $("#txtRoleName").val("");
    $("#txtRoleDescription").val("");
    if ($('#chkRoleIsActive').is(':checked') == false) {
        $(".switchery").click();
        $("#chkText").css("color", "rgb(38, 185, 154)").html("Active");
    }
}

function DeleteRoleDetail(rolecode) {
    if (rolecode != "") {
        if (confirm("Are you sure you want to delete this entry?")) {
            $("#roledelete_" + rolecode).removeClass("fa-trash").addClass("fa-pulse fa-spinner");
            $.ajax({
                type: "Post",
                url: "/Privilege/ProcessToDeleteRole",
                data: '{code: ' + JSON.stringify(rolecode) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        if (data[0] == "true") {
                            DeletePopupMessage("exampleModalLabel", "RoleBody", "RoleFooter", data[1], "rolemodal", "success");
                        }
                        else if (data[0] == "logout") {
                            window.location.reload();
                        }
                        else {
                            $("#roledelete_" + rolecode).removeClass("fa-pulse fa-spinner").addClass("fa-trash");
                            DeletePopupMessage("exampleModalLabel", "RoleBody", "RoleFooter", data[1], "rolemodal", "failed");
                        }
                    }
                }
            });
        }
    }
}

function ModifyRoleDetail(code, name, description, isactive) {
    $("#txtRoleCode").val(code).prop("disabled", true);
    $("#txtRoleName").val(name);
    $("#txtRoleDescription").val(description);

    var isgreen = $(".switchery small").css("left");
    if (isactive == "1") {
        if (isgreen != "40px") {
            $("#chkText").css("color", "rgb(38, 185, 154)").html("Active");
            $(".switchery").click();
        }
    }
    else {
        if (isgreen != "0px") {
            $("#chkText").css("color", "#cc2929").html("InActive");
            $(".switchery").click();
        }
    }
    $("#exampleModalLabel").html("<i class='fa fa-plus' aria-hidden='true'></i>&nbsp;MODIFY DESIGNATION");
    $("#btnRoleSubmit").html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Modify");
    $(".rolemodal").click();
}

function RestoreRoleDetail(rolecode) {
    if (rolecode != "") {
        if (confirm("Are you sure you want to restore this entry?")) {
            var model = {};
            model.RoleCode = rolecode;
            model.IsActive = 1;
            $("#rolerestore_" + rolecode).removeClass("fa-undo").addClass("fa-pulse fa-spinner");
            $.ajax({
                type: "Post",
                url: "/Privilege/ProcessToInsertUpdateRole",
                data: '{model: ' + JSON.stringify(model) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    window.location.reload();
                }
            });
        }
    }
}

//================Designation Section===============

function DesignationSection() {
    SuccessFailMessage("", "", "", "DesignationError", "", "");
    var model = {};
    if (CheckFocusBlankValidation("txtDesignationCode")) return !1;
    if (CheckFocusBlankValidation("txtDesignationName")) return !1;
    if (CheckFocusBlankValidation("txtDesignationDescription")) return !1;

    model.DesignationId = 0;
    model.DesignationCode = $("#txtDesignationCode").val();
    model.DesignationName = $("#txtDesignationName").val();
    model.Description = $("#txtDesignationDescription").val();
    model.IsActive = $('#chkDesignationIsActive').is(':checked') == true ? 1 : 0;

    $("#btnDesignationSubmit").attr("disabled", true).html("Processing...<i class='fa fa-spinner fa-pulse' aria-hidden='true'></i>");
    $.ajax({
        type: "Post",
        url: "/Privilege/ProcessToInsertUpdateDesignation",
        data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#btnDesignationSubmit").attr("disabled", false).html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
            if (data != null) {
                if (data[0] == "true") {
                    ResetDesignationForm();
                    SuccessFailMessage("DesignationBody", data[1], "DesignationFooter", "", "", "success");
                }
                else if (data[0] == "logout") {
                    window.location.reload();
                }
                else {
                    SuccessFailMessage("", "", "", "DesignationError", data[1], "failed");
                }
            }
        }
    });
}

function ResetDesignationForm() {
    $("#txtDesignationCode").val("");
    $("#txtDesignationName").val("");
    $("#txtDesignationDescription").val("");
    if ($('#chkDesignationIsActive').is(':checked') == false) {
        $(".switchery").click();
        $("#chkText").css("color", "rgb(38, 185, 154)").html("Active");
    }
}

function DeleteDesignationDetail(degcode) {
    if (degcode != "") {
        if (confirm("Are you sure you want to delete this entry?")) {
            $("#degdelete_" + degcode).removeClass("fa-trash").addClass("fa-pulse fa-spinner");
            $.ajax({
                type: "Post",
                url: "/Privilege/ProcessToDeleteDesignation",
                data: '{code: ' + JSON.stringify(degcode) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        if (data[0] == "true") {
                            DeletePopupMessage("exampleModalLabel", "DesignationBody", "DesignationFooter", data[1], "designationmodal", "success");
                        }
                        else if (data[0] == "logout") {
                            window.location.reload();
                        }
                        else {
                            $("#degdelete_" + degcode).removeClass("fa-pulse fa-spinner").addClass("fa-trash");
                            DeletePopupMessage("exampleModalLabel", "DesignationBody", "DesignationFooter", data[1], "designationmodal", "failed");
                        }
                    }
                }
            });
        }
    }
}

function ModifyDesignationDetail(code, name, description, isactive) {
    $("#txtDesignationCode").val(code).prop("disabled", true);
    $("#txtDesignationName").val(name);
    $("#txtDesignationDescription").val(description);

    var isgreen = $(".switchery small").css("left");
    if (isactive == "1") {
        if (isgreen != "40px") {
            $("#chkText").css("color", "rgb(38, 185, 154)").html("Active");
            $(".switchery").click();
        }
    }
    else {
        if (isgreen != "0px") {
            $("#chkText").css("color", "#cc2929").html("InActive");
            $(".switchery").click();
        }
    }
    $("#exampleModalLabel").html("<i class='fa fa-plus' aria-hidden='true'></i>&nbsp;MODIFY DESIGNATION");
    $("#btnDesignationSubmit").html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Modify");
    $(".designationmodal").click();
}

function RestoreDesignationDetail(degcode) {
    if (degcode != "") {
        if (confirm("Are you sure you want to restore this entry?")) {
            var model = {};
            model.DesignationCode = degcode;
            model.IsActive = 1;
            $("#degrestore_" + degcode).removeClass("fa-undo").addClass("fa-pulse fa-spinner");
            $.ajax({
                type: "Post",
                url: "/Privilege/ProcessToInsertUpdateDesignation",
                data: '{model: ' + JSON.stringify(model) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    window.location.reload();
                }
            });
        }
    }
}

//======================================================

$(".isactive").click(function () {
    if ($(this).is(":checked")) {
        $("#chkText").css("color", "rgb(38, 185, 154)").html("Active");
    }
    else {
        $("#chkText").css("color", "#cc2929").html("InActive");
    }
});

function ReloadPoupModal() {
    window.location.reload();
}

function DeletePopupMessage(headingid, bodyid, footerid, message, classclick, action) {
    if (action == "success") {
        $("#" + headingid).css("color", "#28a745").html("<i class='fa fa-check-circle text-success'></i>&nbsp;Success");
        $("#" + bodyid).html("<h5 class='text-success text-center'>" + message + "</h5>");
    }
    else {
        $("#" + headingid).css("color", "#dc3545").html("<i class='fa fa-exclamation-triangle text-danger'></i>&nbsp;Failed");
        $("#" + bodyid).html("<h5 class='text-danger text-center'>" + message + "</h5>");
    }
    $("#" + footerid).html("<button type='button' style='cursor: pointer;' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-close' aria-hidden='true'></i>&nbsp;Close</button>");
    $("." + classclick).click();
}

function SuccessFailMessage(bodysecid, successmsg, footersecid, errorsecid, errormsg, action) {
    if (action == "success") {
        $("#" + bodysecid).html("");
        $("#" + bodysecid).html("<h5 class='text-success text-center'>" + successmsg + "</h5>");

        $("#" + footersecid).html("");
        $("#" + footersecid).html("<button type='button' style='cursor: pointer;' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-close' aria-hidden='true'></i>&nbsp;Close</button>");
    }
    else if (action == "failed") {
        $("#" + errorsecid).html("");
        $("#" + errorsecid).html("<p class='text-danger text-center'>" + errormsg + "</h5>");
    }
    else {
        $("#" + errorsecid).html("");
    }
}
