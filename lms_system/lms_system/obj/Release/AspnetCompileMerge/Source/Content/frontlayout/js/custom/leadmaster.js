﻿$(function () {
    BindLeadDetail();
});

function BindLeadDetail() {
    $.ajax({
        type: "Post",
        url: "/LeadMaster/GetAllLeadData",
        //data: '{pname: ' + JSON.stringify(pname) + ',pcode: ' + JSON.stringify(pcode) + ',pdesc: ' + JSON.stringify(pdesc) + ',pwsrate: ' + JSON.stringify(pwsrate) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#tbodyLead").html(response);
        }
    });
}

var iseditlead = false;
$(".addleadmodel").click(function () {
    if (!iseditlead) {
        BindPrductList();
        let strhtml = "<div class='modal-dialog modal-lg' style='margin:3% auto!important;'>"
            + "<div class='modal-content'>"

            //heading section
            + "<div class='modal-header'><h5 class='modal-title'><i class='fa fa-plus'></i>&nbsp;<span class='spanmsg'>LEAD</span></h5>"
            + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"

            //body section
            + "<div class='modal-body' id='LeadFormBody'>"

            + "<div class='row'>"
            + "<div class='col-sm-6'><div class='form-group'><label for='firstname'>First Name <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input id='txtLFirstName' type='text' placeholder='First Name' class='form-control' /></div>"
            + "</div></div>"
            + "<div class='col-sm-6'><div class='form-group'><label for='lastname'>Last Name <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input id='txtLLastName' type='text' placeholder='Last Name' class='form-control' /></div>"
            + "</div></div>"
            + "</div>"

            + "<div class='row'>"
            + "<div class='col-sm-12'><div class='form-group'><label for='emailid'>Email Id <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input id='txtLEmailId' type='text' placeholder='Email Id' class='form-control' /></div>"
            + "</div></div>"
            + "</div>"

            + "<div class='row'>"
            + "<div class='col-sm-6'><div class='form-group'><label for='phonenumber'>Phone Number</label>"
            + "<div><input class='form-control' id='txtLPhoneNumber' placeholder='Phone Number' type='text' /></div>"
            + "</div></div>"
            + "<div class='col-sm-6'><div class='form-group'><label for='mobilenumber'>Mobile Number <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input class='form-control' id='txtLMobileNumber' placeholder='Mobile Number' type='text' /></div>"
            + "</div></div>"
            + "</div>"

            + "<div class='row'>"
            + "<div class='col-sm-6'><div class='form-group'><label for='country'>Country <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><select class='form-control' id='ddlLCountry'><option value=''>Choose Country</option><option value='India'>India</option></select></div>"
            + "</div></div>"
            + "<div class='col-sm-6'><div class='form-group'><label for='state'>State <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><select class='form-control' id='ddlLState'>"
            + "<option value=''>Choose State</option>"
            + "<option value='Andaman and Nicobar Islands'>Andaman and Nicobar Islands</option>"
            + "<option value='Andhra Pradesh'>Andhra Pradesh</option>"
            + "<option value='Arunachal Pradesh'>Arunachal Pradesh</option>"
            + "<option value='Assam'>Assam</option>"
            + "<option value='Bihar'>Bihar</option>"
            + "<option value='Chandigarh'>Chandigarh</option>"
            + "<option value='Chhattisgarh'>Chhattisgarh</option>"
            + "<option value='Dadra and Nagar Haveli and Daman and Diu'>Dadra and Nagar Haveli and Daman and Diu</option>"
            + "<option value='Delhi'>Delhi</option>"
            + "<option value='Goa'>Goa</option>"
            + "<option value='Gujarat'>Gujarat</option>"
            + "<option value='Haryana'>Haryana</option>"
            + "<option value='Himachal Pradesh'>Himachal Pradesh</option>"
            + "<option value='Jharkhand'>Jharkhand</option>"
            + "<option value='Jammu and Kashmir'>Jammu and Kashmir</option>"
            + "<option value='Karnataka'>Karnataka</option>"
            + "<option value='Kerala'>Kerala</option>"
            + "<option value='Ladakh'>Ladakh</option>"
            + "<option value='Lakshadweep'>Lakshadweep</option>"
            + "<option value='Madhya Pradesh'>Madhya Pradesh</option>"
            + "<option value='Maharashtra'>Maharashtra</option>"
            + "<option value='Manipur'>Manipur</option>"
            + "<option value='Meghalaya'>Meghalaya</option>"
            + "<option value='Mizoram'>Mizoram</option>"
            + "<option value='Nagaland'>Nagaland</option>"
            + "<option value='Odisha'>Odisha</option>"
            + "<option value='Puducherry'>Puducherry</option>"
            + "<option value='Punjab'>Punjab</option>"
            + "<option value='Rajasthan'>Rajasthan</option>"
            + "<option value='Sikkim'>Sikkim</option>"
            + "<option value='Tamil Nadu'>Tamil Nadu</option>"
            + "<option value='Telangana'>Telangana</option>"
            + "<option value='Tripura'>Tripura</option>"
            + "<option value='Uttar Pradesh'>Uttar Pradesh</option>"
            + "<option value='Uttarakhand'>Uttarakhand</option>"
            + "<option value='West Bengal'>West Bengal</option>"
            + "</select></div></div></div>"
            + "</div>"

            + "<div class='row'>"
            + "<div class='col-sm-6'><div class='form-group'><label for='productinterestedin'>Product Interested In</label>"
            + "<div class='form-validation'><select class='form-control' id='ddlLProduct'><option value=''>Choose Product</option></select></div>"
            + "</div></div>"
            + "<div class='col-sm-6'><div class='form-group'><label for='leadfrom'>Lead From <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input class='form-control' id='txtLLeadFrom' placeholder='Lead From' type='text' /></div>"
            + "</div></div>"
            + "</div>"

            + "<div class='row'>"
            + "<div class='col-sm-12'><div class='form-group'><label for='remark'>Remark <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><textarea class='form-control' cols='20' id='txtLRemark' placeholder='Remark' rows='2'></textarea></div>"
            + "</div></div>"
            + "</div>"

            

            + "<div class='row'><div class='col-sm-12 text-center text-danger' id='LeadError'></div></div><div class='ln_solid'></div>"

            //submit button
            + "<div class='col-sm-12'><div class='row'><div class='col-sm-6'><p class='text-danger pull-left'>* Mandatory Fields</p></div>"
            + "<div class='col-sm-6 text-right'>"
            + "<button id='btnLeadSubmit' type='submit' class='btn btn-info only-save customer-form-submiter'>"
            + "<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit"
            + "</button></div></div></div>"

            + "</div>"
            + "</div></div>";
        $("#AddLeadModal").html(strhtml);
        $("#ddlLCountry").select2();
        $("#ddlLState").select2();
        $("#ddlLProduct").select2();
    }
});

function BindPrductList() {
    $.ajax({
        type: "Post",
        url: "/LeadMaster/GetAllProductList",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                var productList = response.ProductList;
                $("#ddlLProduct").html("<option value=''>Choose Product</option>");
                $(productList).each(function (index, element) {
                    $("#ddlLProduct").append("<option value='" + element.Value + "'>" + element.Text + "</option>");
                });
            }
        }
    });
}

$(document.body).on('click', "#btnLeadSubmit", function (e) {
    $("#LeadError").html("");
    if (CheckFocusBlankValidation("txtLFirstName")) return !1;
    if (CheckFocusBlankValidation("txtLLastName")) return !1;
    if (CheckFocusBlankValidation("txtLEmailId")) return !1;
    if (CheckEmailValidatoin("txtLEmailId")) return !1;
    if (CheckFocusBlankValidation("txtLMobileNumber")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlLCountry")) return !1;
    if (CheckFocusBlankValidation("ddlLState")) return !1;
    if (CheckFocusBlankValidation("ddlLProduct")) return !1;
    if (CheckFocusBlankValidation("txtLLeadFrom")) return !1;
    if (CheckFocusBlankValidation("txtLRemark")) return !1;
    $(this).html("Please wait...<i class='fa fa-pulse fa-spinner'></i>");

    var leaddel = {};
    leaddel.FirstName = $("#txtLFirstName").val();
    leaddel.LastName = $("#txtLLastName").val();
    leaddel.EmailId = $("#txtLEmailId").val();
    leaddel.PhoneNumber = $("#txtLPhoneNumber").val();
    leaddel.MobileNumber = $("#txtLMobileNumber").val();
    leaddel.Country = $("#ddlLCountry option:selected").val();
    leaddel.State = $("#ddlLState option:selected").val();
    leaddel.InterestedIn = $("#ddlLProduct option:selected").val();
    leaddel.LeadFrom = $("#txtLLeadFrom").val();
    leaddel.Remark = $("#txtLRemark").val();
    leaddel.ProductCode = $("#ddlLProduct option:selected").val();
    leaddel.IsActive = true;

    $.ajax({
        type: "Post",
        url: "/LeadMaster/SaveLeadDetail",
        data: '{lead: ' + JSON.stringify(leaddel) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null && response.length > 0) {
                if (parseInt(response[0]) == 0) {
                    SuccessFailMsg("<i class='fa fa-check-circle' aria-hidden='true'></i>&nbsp;Success", "#26B99A", "Record inserted successfully!", "#26B99A");
                }
                else if (parseInt(response[0]) > 0) {
                    SuccessFailMsg("<i class='fa fa-check-circle' aria-hidden='true'></i>&nbsp;Success", "#26B99A", "Record updated successfully!", "#26B99A");
                }
                $("#tbodyLead").html(response[1]);
            }
            else {
                $("#LeadError").html("Some error occured, Please try again!");
            }

            $("#btnLeadSubmit").html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
        }
    });
});

function SuccessFailMsg(heading, headingcolor, mesg, msgcolor) {
    let strhtml = "<div class='modal-dialog modal-lg' style='margin:5% auto!important;'>"
        + "<div class='modal-content'>"
        + "<div class='modal-header'><h5 class='modal-title' id='exampleModalLabel' style='color:" + headingcolor + "'>" + heading + "</h5>"
        + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"
        + "</div>"
        + "<div class='modal-body'>"
        + "<h5 class='text-center' style='color:" + msgcolor + "'>" + mesg + "</h5>"
        + "</div>"
        + "<div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button></div>"
        + "</div></div>";

    $("#AddLeadModal").html("").html(strhtml);
}

function ModifyLeadDetail(leadid) {
    iseditlead = true;
    let firstname = $("#tdLFirstName_" + leadid).html();
    let lastname = $("#tdLLastName_" + leadid).html();
    let emailid = $("#tdLEmailId_" + leadid).html();
    let phoneno = $("#tdLPhone_" + leadid).html();
    let mobile = $("#tdLMobile_" + leadid).html();
    let product = $("#tdLProduct_" + leadid).html();
    let leadfrom = $("#tdLLeadfrom_" + leadid).html();
    let country = $("#tdLCountry_" + leadid).html();
    let state = $("#tdLState_" + leadid).html();
    let remark = $("#tdLRemark_" + leadid).html();

    $.ajax({
        type: "Post",
        url: "/LeadMaster/GetAllProductList",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                let strhtml = "<div class='modal-dialog modal-lg' style='margin:3% auto!important;'>"
                    + "<div class='modal-content'>"

                    //heading section
                    + "<div class='modal-header'><h5 class='modal-title'><i class='fa fa-plus'></i>&nbsp;<span class='spanmsg'>LEAD</span></h5>"
                    + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"

                    //body section
                    + "<div class='modal-body' id='LeadFormBody'>"

                    + "<div class='row'>"
                    + "<div class='col-sm-6'><div class='form-group'><label for='firstname'>First Name <span class='text-danger'>*</span></label>"
                    + "<div class='form-validation'><input id='txtLFirstName' type='text' placeholder='First Name' class='form-control' value='" + firstname + "' /></div>"
                    + "</div></div>"
                    + "<div class='col-sm-6'><div class='form-group'><label for='lastname'>Last Name <span class='text-danger'>*</span></label>"
                    + "<div class='form-validation'><input id='txtLLastName' type='text' placeholder='Last Name' class='form-control' value='" + lastname + "' /></div>"
                    + "</div></div>"
                    + "</div>"

                    + "<div class='row'>"
                    + "<div class='col-sm-12'><div class='form-group'><label for='emailid'>Email Id <span class='text-danger'>*</span></label>"
                    + "<div class='form-validation'><input id='txtLEmailId' type='text' placeholder='Email Id' class='form-control' value='" + emailid + "' /></div>"
                    + "</div></div>"
                    + "</div>"

                    + "<div class='row'>"
                    + "<div class='col-sm-6'><div class='form-group'><label for='phonenumber'>Phone Number</label>"
                    + "<div><input class='form-control' id='txtLPhoneNumber' placeholder='Phone Number' type='text' value='" + (phoneno == undefined || phoneno == "" ? "" : phoneno) + "'/></div>"
                    + "</div></div>"
                    + "<div class='col-sm-6'><div class='form-group'><label for='mobilenumber'>Mobile Number <span class='text-danger'>*</span></label>"
                    + "<div class='form-validation'><input class='form-control' id='txtLMobileNumber' placeholder='Mobile Number' type='text' value='" + mobile + "' /></div>"
                    + "</div></div>"
                    + "</div>"

                    + "<div class='row'>"
                    + "<div class='col-sm-6'><div class='form-group'><label for='country'>Country <span class='text-danger'>*</span></label>"
                    + "<div class='form-validation'><select class='form-control' id='ddlLCountry'><option value=''>Choose Country</option><option value='India'>India</option></select></div>"
                    + "</div></div>"
                    + "<div class='col-sm-6'><div class='form-group'><label for='state'>State <span class='text-danger'>*</span></label>"
                    + "<div class='form-validation'><select class='form-control' id='ddlLState'>"
                    + "<option value=''>Choose State</option>"
                    + "<option value='Andaman and Nicobar Islands'>Andaman and Nicobar Islands</option>"
                    + "<option value='Andhra Pradesh'>Andhra Pradesh</option>"
                    + "<option value='Arunachal Pradesh'>Arunachal Pradesh</option>"
                    + "<option value='Assam'>Assam</option>"
                    + "<option value='Bihar'>Bihar</option>"
                    + "<option value='Chandigarh'>Chandigarh</option>"
                    + "<option value='Chhattisgarh'>Chhattisgarh</option>"
                    + "<option value='Dadra and Nagar Haveli and Daman and Diu'>Dadra and Nagar Haveli and Daman and Diu</option>"
                    + "<option value='Delhi'>Delhi</option>"
                    + "<option value='Goa'>Goa</option>"
                    + "<option value='Gujarat'>Gujarat</option>"
                    + "<option value='Haryana'>Haryana</option>"
                    + "<option value='Himachal Pradesh'>Himachal Pradesh</option>"
                    + "<option value='Jharkhand'>Jharkhand</option>"
                    + "<option value='Jammu and Kashmir'>Jammu and Kashmir</option>"
                    + "<option value='Karnataka'>Karnataka</option>"
                    + "<option value='Kerala'>Kerala</option>"
                    + "<option value='Ladakh'>Ladakh</option>"
                    + "<option value='Lakshadweep'>Lakshadweep</option>"
                    + "<option value='Madhya Pradesh'>Madhya Pradesh</option>"
                    + "<option value='Maharashtra'>Maharashtra</option>"
                    + "<option value='Manipur'>Manipur</option>"
                    + "<option value='Meghalaya'>Meghalaya</option>"
                    + "<option value='Mizoram'>Mizoram</option>"
                    + "<option value='Nagaland'>Nagaland</option>"
                    + "<option value='Odisha'>Odisha</option>"
                    + "<option value='Puducherry'>Puducherry</option>"
                    + "<option value='Punjab'>Punjab</option>"
                    + "<option value='Rajasthan'>Rajasthan</option>"
                    + "<option value='Sikkim'>Sikkim</option>"
                    + "<option value='Tamil Nadu'>Tamil Nadu</option>"
                    + "<option value='Telangana'>Telangana</option>"
                    + "<option value='Tripura'>Tripura</option>"
                    + "<option value='Uttar Pradesh'>Uttar Pradesh</option>"
                    + "<option value='Uttarakhand'>Uttarakhand</option>"
                    + "<option value='West Bengal'>West Bengal</option>"
                    + "</select></div></div></div>"
                    + "</div>"

                    + "<div class='row'>"
                    + "<div class='col-sm-6'><div class='form-group'><label for='productinterestedin'>Product Interested In</label>"
                    + "<div class='form-validation'><select class='form-control' id='ddlLProduct'></select></div>"
                    + "</div></div>"
                    + "<div class='col-sm-6'><div class='form-group'><label for='leadfrom'>Lead From <span class='text-danger'>*</span></label>"
                    + "<div class='form-validation'><input class='form-control' id='txtLLeadFrom' placeholder='Lead From' type='text' value='" + leadfrom + "' /></div>"
                    + "</div></div>"
                    + "</div>"

                    + "<div class='row'>"
                    + "<div class='col-sm-12'><div class='form-group'><label for='remark'>Remark <span class='text-danger'>*</span></label>"
                    + "<div class='form-validation'><textarea class='form-control' cols='20' id='txtLRemark' placeholder='Remark' rows='2'>" + remark + "</textarea></div>"
                    + "</div></div>"
                    + "</div>"

                    

                    + "<div class='row'><div class='col-sm-12 text-center text-danger' id='LeadError'></div></div><div class='ln_solid'></div>"

                    //submit button
                    + "<div class='col-sm-12'><div class='row'><div class='col-sm-6'><p class='text-danger pull-left'>* Mandatory Fields</p></div>"
                    + "<div class='col-sm-6 text-right'>"
                    + "<button id='btnLeadSubmit' type='submit' class='btn btn-info only-save customer-form-submiter'>"
                    + "<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit"
                    + "</button></div></div></div>"

                    + "</div>"
                    + "</div></div>";
                $("#AddLeadModal").html(strhtml);

                var productList = response.ProductList;
                $("#ddlLProduct").html("<option value=''>Choose Product</option>");
                $(productList).each(function (index, element) {
                    $("#ddlLProduct").append("<option value='" + element.Value + "'>" + element.Text + "</option>");
                });

                $("#ddlLCountry").val(country);
                $("#ddlLState").val(state);
                $("#ddlLProduct").val(product);

                $(".addleadmodel").click();
                iseditlead = false;

                $("#ddlLCountry").select2();
                $("#ddlLState").select2();
                $("#ddlLProduct").select2();
            }
        }
    });
}

//function CheckLeadForm() {
//    if (CheckFocusBlankValidation("FirstName")) return !1;
//    if (CheckFocusBlankValidation("LastName")) return !1;
//    if (CheckFocusBlankValidation("EmailId")) return !1;
//    if (CheckEmailValidatoin("EmailId")) return !1;
//    if (CheckFocusBlankValidation("MobileNumber")) return !1;
//    if (CheckFocusDropDownBlankValidation("Country")) return !1;
//    if (CheckFocusBlankValidation("State")) return !1;
//    if (CheckFocusBlankValidation("ProductCode")) return !1;
//    if (CheckFocusBlankValidation("LeadFrom")) return !1;
//    if (CheckFocusBlankValidation("Remark")) return !1;
//}