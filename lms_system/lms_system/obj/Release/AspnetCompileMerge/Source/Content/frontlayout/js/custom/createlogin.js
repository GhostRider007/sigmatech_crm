﻿//$('#AddCreateLoginModal').on('hidden.bs.modal', function () {
//    window.location.reload();
//});
var iseditlogin = false;
$(function () {
    BindCreatLoginData();
});

function BindCreatLoginData() {
    $.ajax({
        type: "Post",
        url: "/Privilege/BindCreatLoginData",
        //data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#tbodyCreateLogin").html(response);
        }
    });
}

function ModifyCreateLoginDetail(createloginid) {
    iseditlogin = true;
    let employid = $("#tdCLEmpCode_" + createloginid).data("empcode");
    let empfname = $("#tdCLFirstName_" + createloginid).html();
    let empmname = $("#tdCLEmpCode_" + createloginid).data("empmname");
    let emplname = $("#tdCLLastName_" + createloginid).html();
    let roleid = $("#tdCLRoleName_" + createloginid).data("roleid");
    let rolename = $("#tdCLRoleName_" + createloginid).html();
    let empcode = $("#tdCLUserName_" + createloginid).html();
    let isactive = $("#tdCLEmpCode_" + createloginid).data("isactive");
    let fullnamewithid = employid + "," + empfname + " " + empmname + " " + emplname;

    let strhtml = "<div class='modal-dialog modal-lg' style='margin:5% auto!important;'>"
        + "<div class='modal-content'>"

        //heading section
        + "<div class='modal-header'><h5 class='modal-title'><i class='fa fa-plus'></i>&nbsp;<span class='spanmsg'>CREATE LOGIN</span></h5>"
        + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"

        //body section
        + "<div class='modal-body' id='CreateLoginFormBody'>"

        + "<div class='row'>"
        + "<div class='col-sm-6'><div class='form-group'><label for='employeecode'>Employee Code <span class='text-danger'>*</span></label>"
        + "<div class='form-validation'><select class='form-control' id='ddlCLEmployeeList' disabled='disabled'><option value='" + fullnamewithid + "'>" + empcode + "</option></select></div>"
        + "</div></div>"
        + "<div class='col-sm-6'><div class='form-group'><label for='employeename'>Employee Name <span class='text-danger'>*</span></label>"
        + "<div class='form-validation'><input id='txtCLEmployeeName' class='form-control' placeholder='Employee Name' disabled='disabled' value='" + (empfname + " " + empmname + " " + emplname) + "' /></div>"
        + "</div></div>"
        + "</div>"

        + "<div class='row'>"
        + "<div class='col-sm-6'><div class='form-group'><label for='createpassword'>Create Password</label>"
        + "<div class='form-validation'><input id='txtCreatePassword' class='form-control' placeholder='Enter create password' type='password' /></div>"
        + "</div></div>"
        + "<div class='col-sm-6'><div class='form-group'><label for='role'>Role <span class='text-danger'>*</span></label>"
        + "<div class='form-validation'><select class='form-control' id='ddlCLRoleList' disabled='disabled'><option value='" + roleid + "'>" + rolename + "</option></select></div>"
        + "</div></div>"
        + "</div>"

        + "<div class='row'>"
        + "<div class='col-sm-6'><div class='form-group'><label for='status'>&nbsp;</label>"
        + "<div class='form-validation'><button type='button' id='btnIsActive' class='btn btn-toggle " + (isactive == 1 ? "active" : "") + "' data-toggle='button' aria-pressed='true' autocomplete='off'><div class='handle'></div></button></div>"
        + "</div></div>"
        + "</div>"

        + "<div class='row'><div class='col-sm-12 text-center text-danger' id='CreateLoginError'></div></div><div class='ln_solid'></div>"

        //submit button
        + "<div class='col-sm-12'><div class='row'><div class='col-sm-6'><p class='text-danger pull-left'>* Mandatory Fields</p></div>"
        + "<div class='col-sm-6 text-right'>"
        + "<button id='btnCreateLoginSubmit' type='submit' class='btn btn-info only-save customer-form-submiter'>"
        + "<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit"
        + "</button></div></div></div>"

        + "</div>"
        + "</div></div>";
    $("#AddCreateLoginModal").html(strhtml);

    $(".createloginmodal").click();
    iseditlogin = false;
}


$(".createloginmodal").click(function () {
    if (!iseditlogin) {
        let strhtml = "<div class='modal-dialog modal-lg' style='margin:5% auto!important;'>"
            + "<div class='modal-content'>"

            //heading section
            + "<div class='modal-header'><h5 class='modal-title'><i class='fa fa-plus'></i>&nbsp;<span class='spanmsg'>CREATE LOGIN</span></h5>"
            + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"

            //body section
            + "<div class='modal-body' id='CreateLoginFormBody'>"

            + "<div class='row'>"
            + "<div class='col-sm-6'><div class='form-group'><label for='employeecode'>Employee Code <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><select class='form-control' id='ddlCLEmployeeList' onchange = 'EmpDodeChange();'><option value=''>Choose Employee Code</option></select></div>"
            + "</div></div>"
            + "<div class='col-sm-6'><div class='form-group'><label for='employeename'>Employee Name <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input id='txtCLEmployeeName' class='form-control' placeholder='Employee Name' disabled='disabled' /></div>"
            + "</div></div>"
            + "</div>"

            + "<div class='row'>"
            + "<div class='col-sm-6'><div class='form-group'><label for='createpassword'>Create Password</label>"
            + "<div class='form-validation'><input id='txtCreatePassword' class='form-control' placeholder='Enter create password' type='password' /></div>"
            + "</div></div>"
            + "<div class='col-sm-6'><div class='form-group'><label for='role'>Role <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><select class='form-control' id='ddlCLRoleList'><option value=''>Choose Role</option></select></div>"
            + "</div></div>"
            + "</div>"

            + "<div class='row'>"
            + "<div class='col-sm-6'><div class='form-group'><label for='status'>&nbsp;</label>"
            + "<div class='form-validation'><button type='button' id='btnIsActive' class='btn btn-toggle active' data-toggle='button' aria-pressed='true' autocomplete='off'><div class='handle'></div></button></div>"
            + "</div></div>"
            + "</div>"

            + "<div class='row'><div class='col-sm-12 text-center text-danger' id='CreateLoginError'></div></div><div class='ln_solid'></div>"

            //submit button
            + "<div class='col-sm-12'><div class='row'><div class='col-sm-6'><p class='text-danger pull-left'>* Mandatory Fields</p></div>"
            + "<div class='col-sm-6 text-right'>"
            + "<button id='btnCreateLoginSubmit' type='submit' class='btn btn-info only-save customer-form-submiter'>"
            + "<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit"
            + "</button></div></div></div>"

            + "</div>"
            + "</div></div>";
        $("#AddCreateLoginModal").html(strhtml);

        BindEmployeeAndRoleDetail();

        $("#ddlCLEmployeeList").select2();
        $("#ddlCLRoleList").select2();
    }
});

function BindEmployeeAndRoleDetail() {
    $.ajax({
        type: "Post",
        url: "/Privilege/BindEmployeeAndRoleDetail",
        //data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var employeeList = response.EmployeeList;
            var roleList = response.RoleList;

            $("#ddlCLEmployeeList").html("<option value=''>Choose Employee Code</option>");
            $(employeeList).each(function (index, element) {
                $("#ddlCLEmployeeList").append("<option value='" + element.Value + "'>" + element.Text + "</option>");
            });

            $("#ddlCLRoleList").html("<option value=''>Choose Role</option>");
            $(roleList).each(function (index, element) {
                $("#ddlCLRoleList").append("<option value='" + element.Value + "'>" + element.Text + "</option>");
            });
        }
    });
}

$(document.body).on('click', "#btnCreateLoginSubmit", function (e) {
    $("#CreateLoginError").html("");
    if (CheckFocusDropDownBlankValidation("ddlCLEmployeeList")) return !1;
    if (CheckFocusBlankValidation("txtCreatePassword")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlCLRoleList")) return !1;

    var model = {};
    var empselectedcode = $("#ddlCLEmployeeList option:selected").val();
    model.EmpId = empselectedcode.split(',')[0];
    model.EmployeeCode = $("#ddlCLEmployeeList option:selected").text();
    model.EmployeeName = empselectedcode.split(',')[1];
    model.Password = $("#txtCreatePassword").val();
    model.RoleId = $("#ddlCLRoleList option:selected").val();
    model.IsActive = $("#btnIsActive").hasClass("active") == true ? 1 : 0;

    $("#btnCreateLoginSubmit").attr("disabled", true).html("Please wait...<i class='fa fa-pulse fa-spinner'></i>");
    $.ajax({
        type: "Post",
        url: "/Privilege/ProcessToCreateLogin",
        data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != "") {
                SuccessFailMsg("<i class='fa fa-check-circle' aria-hidden='true'></i>&nbsp;Success", "#26B99A", "Record inserted successfully!", "#26B99A");
                $("#tbodyCreateLogin").html(data);
            }
            else {
                $("#CreateLoginError").html("Some error occured, Please try again!");
            }
            $("#btnCreateLoginSubmit").attr("disabled", false).html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
        }
    });
});

$(document.body).on('click', "#btnUpdateCreateLoginSubmit", function (e) {
    $("#CreateLoginError").html("");
    if (CheckFocusDropDownBlankValidation("ddlCLEmployeeList")) return !1;
    if (CheckFocusBlankValidation("txtCreatePassword")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlCLRoleList")) return !1;

    var model = {};
    var empselectedcode = $("#ddlCLEmployeeList option:selected").val();
    model.EmpId = empselectedcode.split(',')[0];
    model.EmployeeCode = $("#ddlCLEmployeeList option:selected").text();
    model.EmployeeName = empselectedcode.split(',')[1];
    model.Password = $("#txtCreatePassword").val();
    model.RoleId = $("#ddlCLRoleList option:selected").val();
    model.IsActive = $("#btnIsActive").hasClass("active") == true ? 1 : 0;

    $("#btnCreateLoginSubmit").attr("disabled", true).html("Please wait...<i class='fa fa-pulse fa-spinner'></i>");
    $.ajax({
        type: "Post",
        url: "/Privilege/ProcessToUpdateCreateLogin",
        data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data == true) {
                SuccessFailMsg("<i class='fa fa-check-circle' aria-hidden='true'></i>&nbsp;Success", "#26B99A", "Record updated successfully!", "#26B99A");
            }
            else {
                $("#CreateLoginError").html("Some error occured, Please try again!");
            }
            $("#btnUpdateCreateLoginSubmit").attr("disabled", false).html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
        }
    });
});

function SuccessFailMsg(heading, headingcolor, mesg, msgcolor) {
    let strhtml = "<div class='modal-dialog modal-lg' style='margin:5% auto!important;'>"
        + "<div class='modal-content'>"
        + "<div class='modal-header'><h5 class='modal-title' id='exampleModalLabel' style='color:" + headingcolor + "'>" + heading + "</h5>"
        + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"
        + "</div>"
        + "<div class='modal-body'>"
        + "<h5 class='text-center' style='color:" + msgcolor + "'>" + mesg + "</h5>"
        + "</div>"
        + "<div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button></div>"
        + "</div></div>";

    $("#AddCreateLoginModal").html("").html(strhtml);
}

function EmpDodeChange() {
    var empselectedcode = $("#ddlCLEmployeeList option:selected").val();
    $("#txtCLEmployeeName").val(empselectedcode.split(',')[1]);
}

$(".isactive").click(function () {
    if ($(this).is(":checked")) {
        $("#chkText").css("color", "rgb(38, 185, 154)").html("Active");
    }
    else {
        $("#chkText").css("color", "#cc2929").html("InActive");
    }
});

function CheckCreateLoginForm() {
    var model = {};
    if (CheckFocusDropDownBlankValidation("EmployeeCode")) return !1;
    if (CheckFocusBlankValidation("txtEmployeeName")) return !1;
    if (CheckFocusBlankValidation("txtEmployeeName")) return !1;
    if (CheckFocusDropDownBlankValidation("RoleId")) return !1;

    var empselectedcode = $("#EmployeeCode option:selected").val();

    model.EmpId = empselectedcode.split(',')[0];
    model.EmployeeCode = $("#EmployeeCode option:selected").text();
    model.EmployeeName = empselectedcode.split(',')[1];
    model.Password = $("#txtCreatePassword").val();
    model.RoleId = $("#RoleId option:selected").val();
    model.IsActive = $('#chkLoginActive').is(':checked') == true ? 1 : 0;

    $("#btnCreateLoginSubmit").attr("disabled", true).html("Processing...<i class='fa fa-spinner fa-pulse' aria-hidden='true'></i>");
    $.ajax({
        type: "Post",
        url: "/Privilege/ProcessToCreateLogin",
        data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#btnRoleSubmit").attr("disabled", false).html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
            if (data != null) {
                if (data[0] == "true") {
                    ResetLoginForm();
                    $("#CreateLoginBody").html("<h5 class='text-success text-center'>" + data[1] + "</h5>");
                    //$("#CreateLoginFooter").css("display", "block");
                    $("#CreateLoginFooter").attr("style", "display:block");
                }
                else if (data[0] == "logout") {
                    window.location.reload();
                }
                else {
                    $("#CreateLoginError").html("<h6>" + data[1] + "</h6>");
                }
            }
        }
    });
}

function ResetLoginForm() {
    $("#txtEmployeeName").val("");
    $("#txtCreatePassword").val("");
}

$("#EmployeeCode").select2();
$("#RoleId").select2();