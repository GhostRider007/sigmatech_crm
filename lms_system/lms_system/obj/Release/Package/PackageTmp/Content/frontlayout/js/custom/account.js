﻿$("#btnBackLogin").click(function () {
    $(".errormsg").addClass("hidden").html("");
    var thisbutton = $("#btnBackLogin");
    var username = $("#txtusername").val();
    var password = $("#txtpassword").val();
    var isrember = null;

    if ($("#ckbRemeber").is(':checked')) { isrember = "true"; }
    else { isrember = "false"; }

    if (username == null || username == "") { $(".usernamesec").css("border-bottom", "1px solid red"); $("#txtusername").focus(); return false; } else { $(".usernamesec").css("border-bottom", "1px solid #b2b2b2"); }
    if (username != "" && (password == null || password == "")) { $(".userpasssec").css("border-bottom", "1px solid red"); $("#txtpassword").focus(); return false; } else { $(".userpasssec").css("border-bottom", "1px solid #b2b2b2"); }

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    $.ajax({
        type: "Post",
        url: "/Account/ProcessBackendLogin",
        data: '{username: ' + JSON.stringify(username) + ',password: ' + JSON.stringify(password) + ',isrember: ' + JSON.stringify(isrember) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    //localStorage.setItem("backloginid", data[1]);
                    window.location.href = data[1];
                }
                else {
                    $(".modelheading").css("color", "#ff0000"); $(".spanmsg").html(" Error Occured !"); $("#iconid").addClass("fa fa-exclamation-triangle"); $(".sucessmsg").html(data[1]);
                    $(".successmessage").click();

                   // $(".errormsg").removeClass("hidden").html(data[1]);
                    //localStorage.removeItem("backloginid");
                    $(thisbutton).html("Login");
                }
            }
        }
    });
});
