﻿$(function () {
    BindProductDetail();
});

function BindProductDetail() {
    $.ajax({
        type: "Post",
        url: "/LeadMaster/BindProductDetail",
        //data: '{pname: ' + JSON.stringify(pname) + ',pcode: ' + JSON.stringify(pcode) + ',pdesc: ' + JSON.stringify(pdesc) + ',pwsrate: ' + JSON.stringify(pwsrate) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#tbodyProduct").html(response);
        }
    });
}

var iseditproduct = false;
$(".addeproductemodal").click(function () {
    if (!iseditproduct) {
        let strhtml = "<div class='modal-dialog modal-lg' style='margin:5% auto!important;'>"
            + "<div class='modal-content'>"
            + "<div class='modal-header'><h5 class='modal-title' id='ProductFormHeading'><i class='fa fa-plus'></i>&nbsp;<span class='spanmsg'>PRODUCT</span></h5>"
            + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
            + "<div class='modal-body' id='ProductFormBody'><div class='row'><div class='col-sm-6'>"
            + "<div class='form-group'><label for='firstname'>Product Name <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input id='txtProductName' type='text' placeholder='Enter Product Name' class='form-control' />"
            + "</div></div></div>"
            + "<div class='col-sm-6'><div class='form-group'><label for='middlename'>Product Code <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input id='txtProductCode' type='text' placeholder='Enter Product Code' class='form-control' /></div>"
            + "</div></div></div>"
            + "<div class='row'><div class='col-sm-12'><div class='form-group'>"
            + "<label for='lastname'>Product Description <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><textarea id='txtDescription' type='text' placeholder='Enter Product Description' class='form-control' rows='3'></textarea>"
            + "</div></div></div></div>"
            + "<div class='row'><div class='col-sm-6'><div class='form-group'>"
            + "<label for='pannumber'>Product Whole Sale Rate <span class='text-danger'>*</span></label>"
            + "<div class='form-validation'><input id='txtPWSRate' type='text' placeholder='Enter Whole Sale Rate' class='form-control' maxlength='10' />"
            + "</div></div></div></div>"
            + "<div class='row'><div class='col-sm-12 text-center text-danger' id='ProductError'></div></div><div class='ln_solid'></div>"
            + "<div class='row'><div class='col-sm-6'><p class='text-danger pull-left'>* Mandatory Fields</p></div>"
            + "<div class='col-sm-6 text-right'>"
            + "<button id='btnProductSubmit' type='submit' class='btn btn-info only-save customer-form-submiter'><i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit</button>"
            + "</div></div></div>"
            + "<div class='modal-footer' id='productmodalfooter' style='display:none;'><div class='col-md-7 col-sm-7 offset-md-5' id='ProductFooter'>"
            + "<button type='button' style='cursor: pointer;' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-close' aria-hidden='true'></i>&nbsp;Close</button>"
            + "</div></div></div></div>";
        $("#AddProductModal").html(strhtml);
    }
});

$(document.body).on('click', "#btnProductSubmit", function (e) {
    if (CheckFocusBlankValidation("txtProductName")) return !1;
    if (CheckFocusBlankValidation("txtProductCode")) return !1;
    if (CheckFocusBlankValidation("txtDescription")) return !1;
    if (CheckFocusBlankValidation("txtPWSRate")) return !1;
    $(this).html("Please wait...<i class='fa fa-pulse fa-spinner'></i>");

    $("#ProductError").html("");
    let pname = $("#txtProductName").val();
    let pcode = $("#txtProductCode").val();
    let pdesc = $("#txtDescription").val();
    let pwsrate = $("#txtPWSRate").val();

    $.ajax({
        type: "Post",
        url: "/LeadMaster/SaveProductDetail",
        data: '{pname: ' + JSON.stringify(pname) + ',pcode: ' + JSON.stringify(pcode) + ',pdesc: ' + JSON.stringify(pdesc) + ',pwsrate: ' + JSON.stringify(pwsrate) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                $("#tbodyProduct").html(response[0]);
                if (response[1] == "0") {
                    SuccessFailMsg("<i class='fa fa-check-circle' aria-hidden='true'></i>&nbsp;Success", "#26B99A", "Record inserted successfully!", "#26B99A");
                }
                else if (parseInt(response[1])>0) {
                    SuccessFailMsg("<i class='fa fa-check-circle' aria-hidden='true'></i>&nbsp;Success", "#26B99A", "Record updated successfully!", "#26B99A");
                }
            }
            else {
                $("#ProductError").html("Some error occured, Please try again!");
            }

            $(this).html("<i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit");
        }
    });
});

function SuccessFailMsg(heading, headingcolor, mesg, msgcolor) {
    let strhtml = "<div class='modal-dialog modal-lg' style='margin:5% auto!important;'>"
        + "<div class='modal-content'>"
        + "<div class='modal-header'><h5 class='modal-title' id='exampleModalLabel' style='color:" + headingcolor + "'>" + heading + "</h5>"
        + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"
        + "</div>"
        + "<div class='modal-body'>"
        + "<h5 class='text-center' style='color:" + msgcolor + "'>" + mesg + "</h5>"
        + "</div>"
        + "<div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button></div>"
        + "</div></div>";

    $("#AddProductModal").html("").html(strhtml);
}

function ModifyProductDetail(name, code, desc, price) {
    iseditproduct = true;

    let tempName = name.replace(/[_\W]+/g, " ");
    let tempCode = code.replace(/[_\W]+/g, " ");
    let tempDesc = desc.replace(/[_\W]+/g, " ");
    let tempPrice = price.replace(/[_\W]+/g, " ");

    let strhtml = "<div class='modal-dialog modal-lg' style='margin:5% auto!important;'>"
        + "<div class='modal-content'>"
        + "<div class='modal-header'><h5 class='modal-title' id='ProductFormHeading'><i class='fa fa-plus'></i>&nbsp;<span class='spanmsg'>PRODUCT</span></h5>"
        + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
        + "<div class='modal-body' id='ProductFormBody'><div class='row'><div class='col-sm-6'>"
        + "<div class='form-group'><label for='firstname'>Product Name <span class='text-danger'>*</span></label>"
        + "<div class='form-validation'><input id='txtProductName' type='text' placeholder='Enter Product Name' class='form-control' value='" + tempName + "' />"
        + "</div></div></div>"
        + "<div class='col-sm-6'><div class='form-group'><label for='middlename'>Product Code <span class='text-danger'>*</span></label>"
        + "<div class='form-validation'><input id='txtProductCode' type='text' placeholder='Enter Product Code' class='form-control' value='" + tempCode + "' disabled='disabled' /></div>"
        + "</div></div></div>"
        + "<div class='row'><div class='col-sm-12'><div class='form-group'>"
        + "<label for='lastname'>Product Description <span class='text-danger'>*</span></label>"
        + "<div class='form-validation'><textarea id='txtDescription' type='text' placeholder='Enter Product Description' class='form-control' rows='3'>" + tempDesc + "</textarea>"
        + "</div></div></div></div>"
        + "<div class='row'><div class='col-sm-6'><div class='form-group'>"
        + "<label for='pannumber'>Product Whole Sale Rate <span class='text-danger'>*</span></label>"
        + "<div class='form-validation'><input id='txtPWSRate' type='text' placeholder='Enter Whole Sale Rate' class='form-control' value='" + tempPrice + "' />"
        + "</div></div></div></div>"
        + "<div class='row'><div class='col-sm-12 text-center text-danger' id='ProductError'></div></div><div class='ln_solid'></div>"
        + "<div class='row'><div class='col-sm-6'><p class='text-danger pull-left'>* Mandatory Fields</p></div>"
        + "<div class='col-sm-6 text-right'>"
        + "<button id='btnProductSubmit' type='submit' class='btn btn-info only-save customer-form-submiter'><i class='fa fa-floppy-o' aria-hidden='true'></i>&nbsp;Submit</button>"
        + "</div></div></div>"
        + "<div class='modal-footer' id='productmodalfooter' style='display:none;'><div class='col-md-7 col-sm-7 offset-md-5' id='ProductFooter'>"
        + "<button type='button' style='cursor: pointer;' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-close' aria-hidden='true'></i>&nbsp;Close</button>"
        + "</div></div></div></div>";
    $("#AddProductModal").html(strhtml);
    $(".addeproductemodal").click();
    iseditproduct = false;
}